FactoryGirl.define do
  sequence :lgu_code do |n|
    n
  end

  factory :lgu_name do
    name "First Name"
    lgu_type_id 1
    region_id 1
    province_id 1
    lgu_code
    income_class "1"
  end
end