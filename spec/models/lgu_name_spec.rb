require 'rails_helper'

RSpec.describe LguName, type: :model do

  let!(:lgu_name) { FactoryGirl.create(:lgu_name) }

  describe "LguName" do
    context "with valid params" do
      it "should create LguName" do
        expect{LguName.create(name: "Test Name", lgu_code: 12345)}.to change{LguName.count}.from(1).to(2)
      end

      it "should not create LguName on lgu_code already exist" do
        LguName.create(name: "Test Name", lgu_code: 12345)
        expect{LguName.create(name: "Test Name", lgu_code: 12345)}.not_to change{LguName.count}
      end

    end

    context "with invalid params" do
      it "should not create LguName" do
        expect{LguName.create(name: nil)}.not_to change{LguName.count}
      end
    end
    
    context "with valid search text" do
      it "should fetch single data" do
        expect( LguName.search("F").first ).to eq(lgu_name)
      end
    end

  end
end
