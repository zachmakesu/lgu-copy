require 'rails_helper'

RSpec.describe IndicatorDatum, type: :model do
  let!(:indicator_datum) { FactoryGirl.create(:indicator_datum) }

  describe "IndicatorDatum" do
    context "with valid params" do
      it "should create IndicatorDatum" do
        expect{IndicatorDatum.create(name: "Test Name")}.to change{IndicatorDatum.count}.from(1).to(2)
      end

      it "should not create IndicatorDatum on name already exist" do
        expect{IndicatorDatum.create(name: "First Name")}.not_to change{IndicatorDatum.count}
      end

    end

    context "with invalid params" do
      it "should not create IndicatorDatum" do
        expect{IndicatorDatum.create(name: nil)}.not_to change{IndicatorDatum.count}
      end
    end
    
    context "with valid search text" do
      it "should fetch single data" do
        expect( IndicatorDatum.search("F").first ).to eq(indicator_datum)
      end
    end

  end
end
