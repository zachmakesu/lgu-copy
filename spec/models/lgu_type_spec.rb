require 'rails_helper'

RSpec.describe LguType, type: :model do

  let!(:lgu_type) { FactoryGirl.create(:lgu_type) }

  describe "LguType" do
    context "with valid params" do
      it "should create LguType" do
        expect{LguType.create(name: "Test Name")}.to change{LguType.count}.from(1).to(2)
      end

      it "should not create LguType on name already exist" do
        expect{LguType.create(name: "First Name")}.not_to change{LguType.count}
      end

    end

    context "with invalid params" do
      it "should not create LguType" do
        expect{LguType.create(name: nil)}.not_to change{LguType.count}
      end
    end
    
    context "with valid search text" do
      it "should fetch single data" do
        expect( LguType.search("F").first ).to eq(lgu_type)
      end
    end

  end
end
