require 'rails_helper'

RSpec.describe ApiKey, type: :model do
  
  it "create api key without access token" do
    key = ApiKey.new
    key.access_token = ''
    refute key.valid?, 'invalid without access_token'
  end

  it 'create valid api key' do
    @valid_token = 'ThisIsAValidAPITokenOfValid32<Len'
    key = ApiKey.new
    key.access_token = @valid_token
    key.save

    expect(@valid_token).not_to eq(key.access_token), 'access_token accessor should be encrypted after setting'
    expect(key.encrypted_access_token).to be_truthy, 'access token should be encrypted'
    expect(key.expires_at).to be_truthy, 'access token should have expiration'
  end

  it 'compare encrypted_access_token and access_token param' do
    @valid_token = 'ThisIsAValidAPITokenOfValid32<Len'
    expect(ApiKey).to respond_to(:secure_compare), 'should respond to .secure_compare'
    key = ApiKey.new
    key.access_token = @valid_token
    key.save
    expect(ApiKey.secure_compare(@valid_token, key.encrypted_access_token)).to be_truthy, 'should successfully compare plain test token with stored encrypted access token'
  end


   it 'scope by validity' do
    @valid_token = 'ThisIsAValidAPITokenOfValid32<Len'

    expect(ApiKey).to respond_to(:valid), 'should respond_to .valid scope'
    key = ApiKey.new
    key.access_token = @valid_token
    key.save
    expect(ApiKey.valid.include?(key)).to be_truthy, 'should included recently created valid key'
    key.update_attribute :expires_at, 14.days.ago
    refute ApiKey.valid.include?(key), 'should included recently created valid key'
  end


end
