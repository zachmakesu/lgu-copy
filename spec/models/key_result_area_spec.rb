require 'rails_helper'

RSpec.describe KeyResultArea, type: :model do

  let!(:key_result_area) { FactoryGirl.create(:key_result_area) }

  describe "KeyResultArea" do
    context "with valid params" do
      it "should create KeyResultArea" do
        expect{KeyResultArea.create(name: "Test Name")}.to change{KeyResultArea.count}.from(1).to(2)
      end

      it "should not create KeyResultArea on name already exist" do
        expect{KeyResultArea.create(name: "First Name")}.not_to change{KeyResultArea.count}
      end

    end

    context "with invalid params" do
      it "should not create KeyResultArea" do
        expect{KeyResultArea.create(name: nil)}.not_to change{KeyResultArea.count}
      end
    end
    
    context "with valid search text" do
      it "should fetch single data" do
        expect( KeyResultArea.search("F").first ).to eq(key_result_area)
      end
    end

  end
end
