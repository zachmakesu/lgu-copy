require "rails_helper"

describe RegionHandler do
  let!(:valid_create_params) { {region: {name: "Test Name"} } }
  let!(:missing_create_params) { { region: {name: nil} }}

  let!(:region) { FactoryGirl.create(:region) }
  let!(:valid_update_params) { {id: region.id, region: {name: "Test Name"} } }
  let!(:missing_update_params) { {id: region.id, region: {name: nil} } }

  describe :create do
    context "with valid params" do
      it "returns created region" do
        handler = RegionHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_create_params[:region][:name])
      end

      it "returns error message if region already exist" do
        RegionHandler.new(valid_create_params).create

        handler = RegionHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = RegionHandler.new(missing_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :update do
    context "with valid params" do
      it "returns updated region" do
        handler = RegionHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_update_params[:region][:name])
      end

      it "returns error message if region already exist" do
        RegionHandler.new(valid_update_params).create
        
        handler = RegionHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = RegionHandler.new(missing_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :delete do
    context "with valid params" do
      it "deletes valid region id" do
        handler = RegionHandler.new({id: region.id}).delete
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
