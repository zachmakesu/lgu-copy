require "rails_helper"

describe IndicatorDatumHandler do
  let!(:valid_create_params) { {indicator_datum: {name: "Test Name"} } }
  let!(:missing_create_params) { { indicator_datum: {name: nil} }}

  let!(:indicator_datum) { FactoryGirl.create(:indicator_datum) }
  let!(:valid_update_params) { {id: indicator_datum.id, indicator_datum: {name: "Test Name"} } }
  let!(:missing_update_params) { {id: indicator_datum.id, indicator_datum: {name: nil} } }

  describe :create do
    context "with valid params" do
      it "returns created indicator_datum" do
        handler = IndicatorDatumHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_create_params[:indicator_datum][:name])
      end

      it "returns error message if indicator_datum already exist" do
        IndicatorDatumHandler.new(valid_create_params).create

        handler = IndicatorDatumHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = IndicatorDatumHandler.new(missing_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :update do
    context "with valid params" do
      it "returns updated indicator_datum" do
        handler = IndicatorDatumHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_update_params[:indicator_datum][:name])
      end

      it "returns error message if indicator_datum already exist" do
        IndicatorDatumHandler.new(valid_update_params).create
        
        handler = IndicatorDatumHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = IndicatorDatumHandler.new(missing_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :delete do
    context "with valid params" do
      it "deletes valid indicator_datum id" do
        handler = IndicatorDatumHandler.new({id: indicator_datum.id}).delete
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
