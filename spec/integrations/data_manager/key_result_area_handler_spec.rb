require "rails_helper"

describe KeyResultAreaHandler do
  let!(:valid_create_params) { {key_result_area: {name: "Test Name"} } }
  let!(:missing_create_params) { { key_result_area: {name: nil} }}

  let!(:key_result_area) { FactoryGirl.create(:key_result_area) }
  let!(:valid_update_params) { {id: key_result_area.id, key_result_area: {name: "Test Name"} } }
  let!(:missing_update_params) { {id: key_result_area.id, key_result_area: {name: nil} } }

  describe :create do
    context "with valid params" do
      it "returns created key_result_area" do
        handler = KeyResultAreaHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_create_params[:key_result_area][:name])
      end

      it "returns error message if key_result_area already exist" do
        KeyResultAreaHandler.new(valid_create_params).create

        handler = KeyResultAreaHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = KeyResultAreaHandler.new(missing_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :update do
    context "with valid params" do
      it "returns updated key_result_area" do
        handler = KeyResultAreaHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_update_params[:key_result_area][:name])
      end

      it "returns error message if key_result_area already exist" do
        KeyResultAreaHandler.new(valid_update_params).create
        
        handler = KeyResultAreaHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name has already been taken")
      end

    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = KeyResultAreaHandler.new(missing_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :delete do
    context "with valid params" do
      it "deletes valid key_result_area id" do
        handler = KeyResultAreaHandler.new({id: key_result_area.id}).delete
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
