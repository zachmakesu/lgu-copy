require "rails_helper"

describe LguNameHandler do
  let!(:valid_create_params) { {lgu_name: {name: "Test Name", lgu_code: 123} } }
  let!(:missing_create_params) { { lgu_name: {name: nil} }}

  let!(:lgu_name) { FactoryGirl.create(:lgu_name) }
  let!(:valid_update_params) { {id: lgu_name.id, lgu_name: {name: "Test Name", lgu_code: 1234} } }
  let!(:missing_update_params) { {id: lgu_name.id, lgu_name: {name: nil} } }

  describe :create do
    context "with valid params" do
      it "returns created lgu_name" do
        handler = LguNameHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_create_params[:lgu_name][:name])
      end

      it "returns error message if lgu_code already exist" do
        LguNameHandler.new(valid_create_params).create

        handler = LguNameHandler.new(valid_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Lgu code has already been taken")
      end
    end

    context "with missing params" do
      it "returns error message" do
        handler = LguNameHandler.new(missing_create_params).create
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank, Lgu code can't be blank")
      end
    end
  end

  describe :update do
    context "with valid params" do
      it "returns updated lgu_name" do
        handler = LguNameHandler.new(valid_update_params).update
        expect(handler.response[:success]).to be_truthy
        expect(handler.response[:details].name).to eq(valid_update_params[:lgu_name][:name])
      end
    end

    context "with invalid credentials" do
      it "returns error message" do
        handler = LguNameHandler.new(missing_update_params).update
        expect(handler.response[:success]).to be_falsey
        expect(handler.response[:details]).to eq("Name can't be blank")
      end
    end
  end

  describe :delete do
    context "with valid params" do
      it "deletes valid lgu_name id" do
        handler = LguNameHandler.new({id: lgu_name.id}).delete
        expect(handler.response[:success]).to be_truthy
      end
    end
  end


end
