module API
  module V1
    module ADMIN
      module DataManager
        class LguCoords < Grape::API
          resource :admin do
            before do
              authorize!(current_user,["admin"])
              allow_cors
            end

            desc "Admin - Current Lgu's coordinates update processed"
            get "/data_manager/update_lgu_coords" do
              background_process = { data: current_user.background_processes.not_finished }
              present background_process, with: Entities::V1::ADMIN::DataManager::LguCoord::Index  
            end

            desc "Admin - Update Lgu's coordinates"
            post "/data_manager/update_lgu_coords" do
              handler = LguCoordHandler.new(current_user).update_lgu_coords
              if handler.response[:success]
                { messages: handler.response[:details] }
              else
                error!({messages: handler.response[:details]},400)
              end
            end

          end
        end
      end
    end
  end
end