module API
  module V1
    module ADMIN
      module DataManager
        class ProvinceManager < Grape::API
          include Grape::Kaminari

          resource :admin do
            before do
              authorize!(current_user,["admin"])
            end

            desc 'Admin - List Provinces'
            paginate per_page: 10, offset: 0
            # params on pagination ?page=1&per_page=10&search=text
            get "/data_manager/provinces" do
              searched = Province.search(params[:search]).order(name: :asc)
              all_data = { data: paginate(searched) }
              present all_data, with: Entities::V1::ADMIN::DataManager::Province::Index, count: Province.count, searched: searched.count
            end

            desc 'Admin - List All Provinces'
            get "/data_manager/provinces/all" do
              searched = Province.all.order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::ADMIN::DataManager::Province::Index, count: Province.count, searched: searched.count
            end

            desc 'Admin - Bulk Delete Provinces'
            # ids=1^2^3                  (integer)
            delete "data_manager/provinces/bulk/:ids" do
              handler = ProvinceHandler.new(params).bulk_delete
              if handler.response[:success]
                searched = Province.all.order(name: :asc)
                all_data = { data: searched }
                present all_data, with: Entities::V1::ADMIN::DataManager::Province::Index, count: Province.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end
            
            desc 'Admin - Specific Province'
            # id                     (integer)
            get "/data_manager/provinces/:id" do
              province = Province.find(params[:id])
              present province, with: Entities::V1::ADMIN::DataManager::Province::ProvinceInfo
            end

            desc 'Admin - Add Province'
            # province[name]         (string)
            # province[geocode]      (string)
            post "/data_manager/provinces" do
              handler = ProvinceHandler.new(params).create
              if handler.response[:success]
                province = Province.find(handler.response[:details].id)
                present province, with: Entities::V1::ADMIN::DataManager::Province::ProvinceInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Update Province'
            # id                   (integer)
            # province[name]       (string)
            # province[geocode]      (string)
            put "/data_manager/provinces/:id/update" do
              handler = ProvinceHandler.new(params).update
              if handler.response[:success]
                province = Province.find(handler.response[:details].id)
                present province, with: Entities::V1::ADMIN::DataManager::Province::ProvinceInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Delete Province'
            # id                     (integer)
            delete "/data_manager/provinces/:id/delete" do
              handler = ProvinceHandler.new(params).delete
              if handler.response[:success]
                searched = Province.search(params[:search]).order(name: :asc)
                all_data = { data: paginate(searched) }
                present all_data, with: Entities::V1::ADMIN::DataManager::Province::Index, count: Province.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

          end
        end
      end
    end
  end
end
