module API
  module V1
    module ADMIN
      module DataManager
        class ImportModule < Grape::API
          include Grape::Kaminari

          resource :admin do
            before do
              authorize!(current_user,["admin"])
              allow_cors
            end

            desc "Admin - Current import_module processed"
            get "/data_manager/import_module" do
              background_process = { data: CsvFile.not_finished }
              present background_process, with: Entities::V1::ADMIN::DataManager::CsvFile::Index  
            end

            desc 'Admin - Upload a FSS csv file'
            params do
              requires :csv_file, type: File, desc: 'FSS CSV file'
            end

            post "/data_manager/import_module" do
              handler = FssCsvFileHandler.new(params, current_user).import_csv_file
              if handler.response[:success]
                { id: handler.response[:details][:id], messages: handler.response[:details][:message] }
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Get Csv preview'
            paginate per_page: 10, offset: 0
            get "/data_manager/csv_preview/:id" do
              @preview = CsvFile.not_finished.find(params[:id])
              @file_name = "db/seeds/json/copy_#{@preview.csv_file_name}_#{@preview.id}.json"
              require 'json'
              data_hash = JSON.parse(File.read("#{@file_name}"))

              searched = if params[:search].present?
                data_hash.select{|key, hash| key["LGU NAME"].include? "#{params[:search].upcase}"}
              else
                data_hash.select{|key, hash| key["LGU NAME"].include? ""}
              end

              results = paginate(Kaminari.paginate_array(searched))
            
              {data: {file_name: @preview.csv_file_name, row: results }, total_rows: data_hash.count, total_results: results.count, total_searched: searched.count} 
            end


          end
        end
      end
    end
  end
end