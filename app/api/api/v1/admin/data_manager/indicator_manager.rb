module API
  module V1
    module ADMIN
      module DataManager
        class IndicatorManager < Grape::API
          include Grape::Kaminari

          resource :admin do
            before do
              authorize!(current_user,["admin"])
            end

            desc 'Admin - List Indicator'
            paginate per_page: 10, offset: 0
            # params on pagination ?page=1&per_page=10&search=text
            get "/data_manager/indicators" do
              searched = IndicatorDatum.includes(:key_result_area, :bench_marks).search(params[:search]).order(name: :asc)
              all_data = { data: paginate(searched) }
              present all_data, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::Index, count: IndicatorDatum.count, searched: searched.count
            end

            desc 'Admin - List all Indicator'
            get "/data_manager/indicators/all" do
              searched = IndicatorDatum.includes(:key_result_area, :bench_marks).order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::Index, count: IndicatorDatum.count, searched: searched.count
            end

            desc 'Admin - Bulk Delete Indicators'
            # ids=1^2^3                  (integer)
            delete "data_manager/indicators/bulk/:ids" do
              handler = IndicatorDatumHandler.new(params).bulk_delete
              if handler.response[:success]
                searched = IndicatorDatum.includes(:key_result_area, :bench_marks).order(name: :asc)
                all_data = { data: searched }
                present all_data, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::Index, count: IndicatorDatum.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Specific Indicator'
            # id                     (integer)
            get "/data_manager/indicators/:id" do
              indicator_datum = IndicatorDatum.includes(:key_result_area, :bench_marks).find(params[:id])
              present indicator_datum, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::IndicatorDatumInfo
            end

            desc 'Admin - Add Indicator'
            # indicator_datum[key_result_area_id]  (integer)
            # indicator_datum[name]                (string)
            # indicator_datum[formula]             (string)
            post "/data_manager/indicators" do
              handler = IndicatorDatumHandler.new(params).create
              if handler.response[:success]
                indicator_datum = IndicatorDatum.includes(:key_result_area, :bench_marks).find(handler.response[:details].id)
                present indicator_datum, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::IndicatorDatumInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Update Indicator'
            # id                                          (integer)
            # indicator_datum[key_result_area_id]         (integer)
            # indicator_datum[name]                       (string)
            # indicator_datum[formula]                    (string)
            put "/data_manager/indicators/:id/update" do
              handler = IndicatorDatumHandler.new(params).update
              if handler.response[:success]
                indicator_datum = IndicatorDatum.includes(:key_result_area, :bench_marks).find(handler.response[:details].id)
                present indicator_datum, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::IndicatorDatumInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Delete Indicator'
            # id                     (integer)
            delete "/data_manager/indicators/:id/delete" do
              handler = IndicatorDatumHandler.new(params).delete
              if handler.response[:success]
                searched = IndicatorDatum.search(params[:search]).order(name: :asc)
                all_data = { data: paginate(searched) }
                present all_data, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::Index, count: IndicatorDatum.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc "Admin - Add Indicator's Income class"
            # id                          (integer)
            # bench_mark[income_class]    (string)
            # bench_mark[value]           (string)

            post "/data_manager/indicators/:id/income_classes" do
              handler = IndicatorDatumHandler.new(params).create_income_class
              if handler.response[:success]
                indicator_datum = IndicatorDatum.includes(:key_result_area, :bench_marks).find(params[:id])
                present indicator_datum, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::IndicatorDatumInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc "Admin - Update Indicator's Income class"
            # id                          (integer)
            # income_class_id             (integer)
            # bench_mark[income_class]    (string)
            # bench_mark[value]           (string)
            put "/data_manager/indicators/:id/income_classes/:income_class_id/update" do
              handler = IndicatorDatumHandler.new(params).update_income_class
              if handler.response[:success]
                indicator_datum = IndicatorDatum.includes(:key_result_area, :bench_marks).find(params[:id])
                present indicator_datum, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::IndicatorDatumInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc "Admin - Delete Indicator's Income class"
            # id                          (integer)
            # income_class_id             (integer)
            delete "/data_manager/indicators/:id/income_classes/:income_class_id/delete" do
              handler = IndicatorDatumHandler.new(params).delete_income_class
              if handler.response[:success]
                indicator_datum = IndicatorDatum.includes(:key_result_area, :bench_marks).find(params[:id])
                present indicator_datum, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::IndicatorDatumInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

          end
        end
      end
    end
  end
end
