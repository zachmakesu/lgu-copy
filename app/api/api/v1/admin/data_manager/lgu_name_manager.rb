module API
  module V1
    module ADMIN
      module DataManager
        class LguNameManager < Grape::API
          include Grape::Kaminari

          resource :admin do
            before do
              authorize!(current_user,["admin"])
            end

            desc 'Admin - List LGU Names'
            paginate per_page: 10, offset: 0
            # params on pagination ?page=1&per_page=10&search=text
            get "/data_manager/lgu_names" do
              searched = LguName.includes(:lgu_type, :region, :province).search(params[:search]).order(name: :asc)
              all_data = { data: paginate(searched) }
              present all_data, with: Entities::V1::ADMIN::DataManager::LguName::Index, count: LguName.count, searched: searched.count
            end

            desc 'Admin - List all LGU Names'
            get "/data_manager/lgu_names/all" do
              searched = LguName.includes(:lgu_type, :region, :province).order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::ADMIN::DataManager::LguName::Index, count: LguName.count, searched: searched.count
            end

            desc 'Admin - Bulk Delete LGU Names'
            # ids=1^2^3                  (integer)
            delete "data_manager/lgu_names/bulk/:ids" do
              handler = LguNameHandler.new(params).bulk_delete
              if handler.response[:success]
                searched = LguName.includes(:lgu_type, :region, :province).order(name: :asc)
                all_data = { data: searched }
                present all_data, with: Entities::V1::ADMIN::DataManager::LguName::Index, count: LguName.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Specific LGU Name'
            # id                     (integer)
            get "/data_manager/lgu_names/:id" do
              lgu_name = LguName.includes(:lgu_type, :region, :province).find(params[:id])
              present lgu_name, with: Entities::V1::ADMIN::DataManager::LguName::LguNameInfo
            end

            desc 'Admin - Add LGU Name'
            # lgu_name[name]         (string)
            # lgu_name[lgu_type_id]  (integer)
            # lgu_name[region_id]    (integer)
            # lgu_name[province_id]  (integer)
            # lgu_name[lgu_code]     (integer)
            # lgu_name[income_class] (string)
            # lgu_name[latitude]     (string)
            # lgu_name[longitude]    (string)
            # image                  (file) extensions(jpg|jpeg|png)
            # background_image      (file) extensions(jpg|jpeg|png)
            post "/data_manager/lgu_names" do
              handler = LguNameHandler.new(params).create
              if handler.response[:success]
                lgu_name = LguName.includes(:lgu_type, :region, :province).find(handler.response[:details].id)
                present lgu_name, with: Entities::V1::ADMIN::DataManager::LguName::LguNameInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Update LGU Name'
            # id                     (integer)
            # lgu_name[name]         (string)
            # lgu_name[lgu_type_id]  (integer)
            # lgu_name[region_id]    (integer)
            # lgu_name[province_id]  (integer)
            # lgu_name[lgu_code]     (integer)
            # lgu_name[income_class] (string)
            # lgu_name[latitude]     (string)
            # lgu_name[longitude]    (string)
            put "/data_manager/lgu_names/:id/update" do
              handler = LguNameHandler.new(params).update
              if handler.response[:success]
                lgu_name = LguName.includes(:lgu_type, :region, :province).find(handler.response[:details].id)
                present lgu_name, with: Entities::V1::ADMIN::DataManager::LguName::LguNameInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Update LGU Name background_image and image'
            # id                     (integer)
            # image        (file) extensions(jpg|jpeg|png)
            # background_image      (file) extensions(jpg|jpeg|png)
            put "/data_manager/lgu_names/:id/update_images" do
              handler = LguNameHandler.new(params).update_images
              if handler.response[:success]
                lgu_name = LguName.includes(:lgu_type, :region, :province).find(handler.response[:details].id)
                present lgu_name, with: Entities::V1::ADMIN::DataManager::LguName::LguNameInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Delete LGU Name'
            # id                     (integer)
            delete "/data_manager/lgu_names/:id/delete" do
              handler = LguNameHandler.new(params).delete
              if handler.response[:success]
                searched = LguName.includes(:lgu_type, :region, :province).search(params[:search]).order(name: :asc)
                all_data = { data: paginate(searched) }
                present all_data, with: Entities::V1::ADMIN::DataManager::LguName::Index, count: LguName.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

          end
        end
      end
    end
  end
end
