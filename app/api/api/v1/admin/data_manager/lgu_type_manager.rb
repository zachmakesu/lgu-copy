module API
  module V1
    module ADMIN
      module DataManager
        class LguTypeManager < Grape::API
          include Grape::Kaminari
          resource :admin do
            before do
              authorize!(current_user,["admin"])
            end

            desc 'Admin - List LGU types'
            paginate per_page: 10, offset: 0
            # params on pagination ?page=1&per_page=10&search=text
            get "/data_manager/lgu_types" do
              searched = LguType.search(params[:search]).order(name: :asc)
              all_data = { data: paginate(searched) }
              present all_data, with: Entities::V1::ADMIN::DataManager::LguType::Index, count: LguType.count, searched: searched.count
            end

            desc 'Admin - List All LGU types'
            get "/data_manager/lgu_types/all" do
              searched = LguType.all.order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::ADMIN::DataManager::LguType::Index, count: LguType.count, searched: searched.count
            end

            desc 'Admin - Bulk Delete LGU types'
            # ids=1^2^3                  (integer)
            delete "data_manager/lgu_types/bulk/:ids" do
              handler = LguTypeHandler.new(params).bulk_delete
              if handler.response[:success]
                searched = LguType.all.order(name: :asc)
                all_data = { data: searched }
                present all_data, with: Entities::V1::ADMIN::DataManager::LguType::Index, count: LguType.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Specific LGU type'
            # id                     (integer)
            get "/data_manager/lgu_types/:id" do
              lgu_type = LguType.find(params[:id])
              present lgu_type, with: Entities::V1::ADMIN::DataManager::LguType::LguTypeInfo
            end

            desc 'Admin - Add LGU type'
            # lgu_type[name]         (string)
            post "/data_manager/lgu_types" do
              handler = LguTypeHandler.new(params).create
              if handler.response[:success]
                lgu_type = LguType.find(handler.response[:details].id)
                present lgu_type, with: Entities::V1::ADMIN::DataManager::LguType::LguTypeInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Update LGU type'
            # id                     (integer)
            # lgu_type[name]         (string)
            put "/data_manager/lgu_types/:id/update" do
              handler = LguTypeHandler.new(params).update
              if handler.response[:success]
                lgu_type = LguType.find(handler.response[:details].id)
                present lgu_type, with: Entities::V1::ADMIN::DataManager::LguType::LguTypeInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Delete LGU type'
            # id                     (integer)
            delete "/data_manager/lgu_types/:id/delete" do
              handler = LguTypeHandler.new(params).delete
              if handler.response[:success]
                searched = LguType.search(params[:search]).order(name: :asc)
                all_data = { data: paginate(searched) }
                present all_data, with: Entities::V1::ADMIN::DataManager::LguType::Index, count: LguType.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

          end
        end
      end
    end
  end
end
