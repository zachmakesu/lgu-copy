module API
  module V1
    module ADMIN
      module DataManager
        class KeyResultAreaManager < Grape::API
          include Grape::Kaminari

          resource :admin do
            before do
              authorize!(current_user,["admin"])
            end

            desc 'Admin - List KeyResultAreas'
            paginate per_page: 10, offset: 0
            # params on pagination ?page=1&per_page=10&search=text
            get "/data_manager/key_result_areas" do
              searched = KeyResultArea.search(params[:search]).order(name: :asc)
              all_data = { data: paginate(searched) }
              present all_data, with: Entities::V1::ADMIN::DataManager::KeyResultArea::Index, count: KeyResultArea.count, searched: searched.count
            end

            desc 'Admin - List KeyResultAreas'
            get "/data_manager/key_result_areas/all" do
              searched = KeyResultArea.order(name: :asc)
              all_data = { data: searched }
              present all_data, with: Entities::V1::ADMIN::DataManager::KeyResultArea::Index, count: KeyResultArea.count, searched: searched.count
            end

            desc 'Admin - Bulk Delete KeyResultAreas'
            # ids=1^2^3                  (integer)
            delete "data_manager/key_result_areas/bulk/:ids" do
              handler = KeyResultAreaHandler.new(params).bulk_delete
              if handler.response[:success]
                searched = KeyResultArea.order(name: :asc)
                all_data = { data: searched }
                present all_data, with: Entities::V1::ADMIN::DataManager::KeyResultArea::Index, count: KeyResultArea.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Specific KeyResultArea'
            # id                     (integer)
            get "/data_manager/key_result_areas/:id" do
              key_result_area = KeyResultArea.find(params[:id])
              present key_result_area, with: Entities::V1::ADMIN::DataManager::KeyResultArea::KeyResultAreaInfo
            end

            desc 'Admin - Add KeyResultArea'
            # key_result_area[indicator_type]         (string) values(financial, non_financial)
            # key_result_area[name]         (string)
            post "/data_manager/key_result_areas" do
              handler = KeyResultAreaHandler.new(params).create
              if handler.response[:success]
                key_result_area = KeyResultArea.find(handler.response[:details].id)
                present key_result_area, with: Entities::V1::ADMIN::DataManager::KeyResultArea::KeyResultAreaInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Update KeyResultArea'
            # id                   (integer)
            # key_result_area[indicator_type]         (string) values(financial, non_financial)
            # key_result_area[name]         (string)
            put "/data_manager/key_result_areas/:id/update" do
              handler = KeyResultAreaHandler.new(params).update
              if handler.response[:success]
                key_result_area = KeyResultArea.find(handler.response[:details].id)
                present key_result_area, with: Entities::V1::ADMIN::DataManager::KeyResultArea::KeyResultAreaInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Admin - Delete KeyResultArea'
            # id                     (integer)
            delete "/data_manager/key_result_areas/:id/delete" do
              handler = KeyResultAreaHandler.new(params).delete
              if handler.response[:success]
                searched = KeyResultArea.search(params[:search]).order(name: :asc)
                all_data = { data: paginate(searched) }
                present all_data, with: Entities::V1::ADMIN::DataManager::KeyResultArea::Index, count: KeyResultArea.count, searched: searched.count
              else
                error!({messages: handler.response[:details]},400)
              end
            end

          end
        end
      end
    end
  end
end
