module API
  module V1
    module ADMIN
      module Settings
        class UserManager < Grape::API
          #include Grape::Kaminari

          resource :admin do
            before do
              authorize!(current_user,["admin"])
            end
            
            desc 'Admin - List all Users for User Manager'
            #paginate per_page: 10, offset: 0
            # params on pagination ?page=1&per_page=10&search=text
            get "/user_manager" do
              searched = User.except_self(current_user).search(params[:search])
              users = { data: searched }
              present users, with: Entities::V1::ADMIN::Settings::UserAccount::Index, count: User.except_self(current_user).count, searched: searched.count
            end

            desc 'Get a user for User Manager'
            # employee_id (string)
            get "/user_manager/:employee_id" do
              user = User.not_deleted.find_by(employee_id: params[:employee_id])
              if user
                present user , with: Entities::V1::ADMIN::Settings::UserAccount::UserInfo, current_user: current_user
              else
                error!({messages: "Invalid user"},400)
              end
            end

            desc 'Get a user for User Manager'
            # employee_id (string)
            post "/user_manager/:employee_id/deactivate" do
              handler = UserManagerHandler.new(params).deactivate
              if handler.response[:success]
                present handler.response[:details], with: Entities::V1::ADMIN::Settings::UserAccount::UserInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc 'Get a user for User Manager'
            # employee_id (string)
            post "/user_manager/:employee_id/activate" do
              handler = UserManagerHandler.new(params).activate
              if handler.response[:success]
                present handler.response[:details], with: Entities::V1::ADMIN::Settings::UserAccount::UserInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc "Admin - Add A User"
            # email               (string)
            # first_name          (string)
            # last_name           (string
            # role                (string)  values("admin", "normal")
            # default password value is "password123"
            # image               (file)    extensions(jpg|jpeg|png)
            post "/user_manager" do
              handler = UserManagerHandler.new(params).create_user
              if handler.response[:success]
                present handler.response[:details], with: Entities::V1::ADMIN::Settings::UserAccount::UserInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

            desc "Admin - Update A User"
            # employee_id         (string)
            # password            (string)
            # email               (string)
            # first_name          (string)
            # last_name           (string
            # role                (string)  values("admin", "normal")
            # image               (file)    extensions(jpg|jpeg|png)
            post "/user_manager/:employee_id/update" do
              handler = UserManagerHandler.new(params).update_user
              if handler.response[:success]
                present handler.response[:details], with: Entities::V1::ADMIN::Settings::UserAccount::UserInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end


          end
        end
      end
    end
  end
end
