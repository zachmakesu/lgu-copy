module API
  module V1
    module ADMIN
      module Settings
        class AgencyInfos < Grape::API
          resource :admin do
            before do
              @model_fields = [ 
              :telephone_number,
              :mobile_number,
              :email,
              :address,
              :about_us
              ]
              @model = :agency_info
            end
      
            desc "Admin - Get Agency Info"
            get "/agency_info" do
              present AgencyInfo.first, with: Entities::V1::ADMIN::Settings::AgencyInfo::Index
            end

            desc "Admin - Update Agency Info"
            # agency_info[telephone_number]
            # agency_info[mobile_number]
            # agency_info[email]
            # agency_info[address]
            # agency_info[about_us]
            put "/agency_info" do
              authorize!(current_user,["admin"])
              AgencyInfo.first.update permited_params(@model, @model_fields)
              present AgencyInfo.first, with: Entities::V1::ADMIN::Settings::AgencyInfo::Index
            end

          end
        end
      end
    end
  end
end
