module API
  module V1
    module ADMIN
      module LguNameDataManager
        class LguNameData < Grape::API
          include Grape::Kaminari
          resource :admin do
            before do
              authorize!(current_user,["admin"])
            end

            desc "Admin - List LGU Name's Data"
            # id                                       (integer)
            get "/lgu_name_data_manager/lgu_names/:id" do
              lgu_name = { data: LguName.includes(data_containers: [{ indicator_datum: [:key_result_area] } , :raw_data]).find(params[:id]) } 
              overall_rating_ids = IndicatorDatum.overall_rating.map(&:id)
              overall_ratings = lgu_name[:data].data_containers.where(indicator_datum_id: overall_rating_ids)
              present lgu_name, with: Entities::V1::ADMIN::LguNameDataManager::LguNameDatum::Index, overall_ratings: overall_ratings
            end

            desc "Admin - Specific LGU Name's Data container"
            # id                                       (integer)
            # container_id                             (integer)
            get "/lgu_name_data_manager/lgu_names/:id/data_containers/:container_id" do
              lgu_name  = LguName.find(params[:id])
              container = lgu_name.data_containers.find(params[:container_id])
              present container, with: Entities::V1::ADMIN::LguNameDataManager::DataContainer::DataContainerInfo
            end


            desc "Admin - Update List LGU Name's Data container"
            # id                                       (integer)
            # container_id                             (integer)
            # data_container[score_visualization]      (string) values ("pie", "doughnut", "line", "bar")
            # data_container[comparison_visualization] (string) values ("pie", "doughnut", "line", "bar")
            # data_container[benchmark_visualization]  (string) values ("pie", "doughnut", "line", "bar")
            put "/lgu_name_data_manager/lgu_names/:id/data_containers/:container_id/update" do
              handler = LguNameDataHandler.new(params).update
              if handler.response[:success]
                container = DataContainer.find(handler.response[:details].id)
                present container, with: Entities::V1::ADMIN::LguNameDataManager::DataContainer::DataContainerInfo
              else
                error!({messages: handler.response[:details]},400)
              end
            end

          end
        end
      end
    end
  end
end
