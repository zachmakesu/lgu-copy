module API
  module V1
    module ADMIN
      class AuthLess < Grape::API
        mount API::V1::ADMIN::Login
        mount API::V1::ADMIN::ForgotPassword
        #mount API::V1::Register
      end
    end
  end
end
