module API
  module V1
    module ADMIN
      class ForgotPassword < Grape::API

        resource :admin do
          desc "LGU forgot password"
          params do
            requires :email, type: String, desc: 'User email'
          end

          # email (string)
          post "/forgot_password" do
            handler = ForgotPasswordHandler.new(params).reset_password
            if handler.response[:success]
             {messages: handler.response[:details]}
            else
              error!({messages: handler.response[:details]},400)
            end
          end
          
        end
      end
    end
  end
end
