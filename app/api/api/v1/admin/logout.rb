module API
  module V1
    module ADMIN
      class Logout < Grape::API

        resource :admin do
          desc 'Destroy user access token'
          params do
            requires :employee_id, type: String, desc: 'User employee_id'
          end

          delete "/logout" do
            if current_user.present?
              params[:employee_id] = current_user.employee_id
              handler = SessionHandler.new(params).logout
              if handler.response[:success]
               {
                messages: handler.response[:details][:messages]
               }
              else
                error!({messages: handler.response[:details]},400)
              end
            else
              {
                messages: "Please log in"
              }
            end
          end
        end
      end
    end
  end
end
