module API
  module V1
    module ADMIN
      class AuthRequiredAdmin < Grape::API

        helpers do
          def authenticate!
            #return if Rails.env.development?
            auth_header = headers['Authorization']

            # Check if Authorization header is present, else return 401
            error!("401 Error: Missing Authorization header", 401) if auth_header.blank?

            # generated_sig = HmacHandler.signature_from(request.path, params)
            # Authorization header is present, check if it conforms to our specs
            error!("401 Error: Invalid Authorization header", 401) unless is_header_valid?(auth_header)
          end

          def is_header_valid? auth
            if /\ALGU ([\w]+):([\w\+\=]+)\z/ =~ auth
              employee_id = $1
              signature = $2
              token = params[:access_token]
              # puts "\n####{request.path} params : #{params}\n###"
              # puts "#{request.path} is_token_valid? : #{is_token_valid?(employee_id, token)}"
              # puts "#{request.path} is_signature_authentic? : #{is_signature_authentic?(signature)}"
              (return true if Rails.env.development? || Rails.env.test?) if !User.not_deleted.find_by(employee_id: employee_id).nil?
              is_token_valid?(employee_id, token) && is_signature_authentic?(signature)
            else
              false
            end
          end

          def is_token_valid?(employee_id, token)
            if !User.find_by(employee_id: employee_id).nil?
              return api_key = User.find_by(employee_id: employee_id).api_keys.valid.detect{|a| ApiKey.secure_compare(token, a.encrypted_access_token) }
            else
              return false
            end
            ApiKey.secure_compare(token, api_key.encrypted_access_token)
          end

          def is_signature_authentic?(sig)
            generated_sig = HmacHandler.signature_from(request.path, params)
            Rails.logger.debug "#{request.path} expected: #{generated_sig}; actual: #{sig}" if Rails.env.staging?
            Rails.logger.debug "params: #{params}" if Rails.env.staging?
            HmacHandler.secure_compare(generated_sig, sig)
          end

          def current_user
            auth = headers['Authorization']
            # puts "request: #{request.path}"
            # puts "auth header: #{auth}"
            # puts "is_header_valid? #{is_header_valid?(auth)}"
            return nil unless is_header_valid? auth
            if /\LGU ([\w]+):([\w\+\=]+)\z/ =~ auth
              employee_id = $1
              # puts "employee_id: #{employee_id}"
              if @current_user
                @current_user
              else
                (@current_user = User.not_deleted.find_by(employee_id: employee_id)) ? @current_user : nil
              end
            end
          end
        end

        before do
          authenticate!
        end

        # Mount all endpoints that require authentication
        mount API::V1::ADMIN::Logout
        mount API::V1::ADMIN::Settings::UserManager
        mount API::V1::ADMIN::Settings::AgencyInfos
        mount API::V1::ADMIN::DataManager::ImportModule
        mount API::V1::ADMIN::DataManager::LguCoords
        mount API::V1::ADMIN::DataManager::LguTypeManager
        mount API::V1::ADMIN::DataManager::RegionManager
        mount API::V1::ADMIN::DataManager::ProvinceManager
        mount API::V1::ADMIN::DataManager::LguNameManager
        mount API::V1::ADMIN::DataManager::KeyResultAreaManager
        mount API::V1::ADMIN::DataManager::IndicatorManager
        mount API::V1::ADMIN::LguNameDataManager::LguNameData

        
      end
    end
  end
end
