module API
  module V1
    module ADMIN
      class Base < Grape::API
        rescue_from :all, backtrace: true do |e|
          ExceptionNotifier.notify_exception(e)
          Rails.logger.debug e if Rails.env.development?
          error!({ error: 'Internal server error', backtrace: e.backtrace }, 500 )
        end

        rescue_from ActiveRecord::RecordNotFound do |e|
          rack_response('{ "status": 404, "message": "Requested resource not found" }', 404)
        end

        rescue_from ActiveRecord::InvalidForeignKey do |e|
          rack_response('{ "status": 404, "message": "Parent record not found" }', 404)
        end

        rescue_from ActiveRecord::RecordInvalid do |e|
          rack_response('{ "status": 404, "message": "Invalid create or update request" }', 404)
        end

        rescue_from ArgumentError do |e|
          rack_response('{ "status": 404, "message": "Invalid values on optional fields " }', 422)
        end

        rescue_from ActionController::ParameterMissing do |e|
          rack_response('{ "status": 404, "message": "Invalid required parameters" }', 422)
        end

        resource :admin do
          get do
            {
              version: version
            }
          end

          post "/authenticated" do
            api_key = ApiKey.valid.detect{|a| ApiKey.secure_compare(params[:access_token], a.encrypted_access_token) }
            {
                authenticated: api_key.present?
            }
          end

        end

        mount API::V1::ADMIN::AuthLess
        mount API::V1::ADMIN::AuthRequiredAdmin
      end
    end
  end
end