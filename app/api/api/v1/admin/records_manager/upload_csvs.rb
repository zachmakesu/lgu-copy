module API
  module V1
    module ADMIN
      module RecordsManager
        class UploadCsvs < Grape::API
          resource :records_manager do
            before do
              authorize!(current_user,["admin"])
            end
            
            desc 'Upload CSVs'
            get do
              "LGU ADMIN"
            end
          end
        end
      end
    end
  end
end
