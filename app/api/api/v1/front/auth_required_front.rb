module API
  module V1
    module FRONT
      class AuthRequiredFront < Grape::API

        helpers do
          def authenticate!
            #return if Rails.env.development?
            auth_header = headers['Authorization']

            # Check if Authorization header is present, else return 401
            error!("401 Error: Missing Authorization header", 401) if auth_header.blank?

            # generated_sig = HmacHandler.signature_from(request.path, params)
            # Authorization header is present, check if it conforms to our specs
            error!("401 Error: Invalid Authorization header", 401) unless is_header_valid?(auth_header)

          end

          def is_header_valid? auth
            if /\ALGU ([\w]+)\z/ =~ auth
              #token = $1
              signature = $1
              #token = params[:access_token]
              return true if Rails.env.development? || Rails.env.test?
              #is_token_valid?(employee_id, token) && is_signature_authentic?(signature)
              is_signature_authentic?(signature)
            else
              false
            end
          end

          # def is_token_valid?(employee_id, token)
          #   if !User.find_by(employee_id: employee_id).nil?
          #     return api_key = User.find_by(employee_id: employee_id).api_keys.valid.detect{|a| ApiKey.secure_compare(token, a.encrypted_access_token) }
          #   else
          #     return false
          #   end
          #   ApiKey.secure_compare(token, api_key.encrypted_access_token)
          # end

          def is_signature_authentic?(sig)
            generated_sig = HmacHandler.signature_from(request.path, params)
            Rails.logger.debug "#{request.path} expected: #{generated_sig}; actual: #{sig}" if Rails.env.staging?
            Rails.logger.debug "params: #{params}" if Rails.env.staging?
            HmacHandler.secure_compare(generated_sig, sig)
          end
          
        end

        before do
          authenticate!
        end

        # Mount all endpoints that require authentication
        mount API::V1::FRONT::LguNames
        mount API::V1::FRONT::KeyResultAreas
        mount API::V1::FRONT::Indicators
        mount API::V1::FRONT::Provinces
        mount API::V1::FRONT::Regions
        mount API::V1::FRONT::LguTypes
        mount API::V1::FRONT::AgencyInfos
        mount API::V1::FRONT::ContactUs

      end
    end
  end
end
