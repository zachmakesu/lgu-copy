module API
  module V1
    module FRONT
      class Provinces < Grape::API
        include Grape::Kaminari

        resource :front do

          desc 'Front - List Provinces'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get "/provinces" do
            searched = Province.search(params[:search]).order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::ADMIN::DataManager::Province::Index, count: Province.count, searched: searched.count
          end

          desc 'Front - List All Provinces'
          get "/provinces/all" do
            searched = Province.order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::ADMIN::DataManager::Province::Index, count: Province.count, searched: searched.count
          end

          desc 'Front - Specific Province'
          # id                     (integer)
          get "/provinces/:id" do
            province = Province.find(params[:id])
            present province, with: Entities::V1::ADMIN::DataManager::Province::ProvinceInfo
          end
          
        end
      end
    end
  end
end
