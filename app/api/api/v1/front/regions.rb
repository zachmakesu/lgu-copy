module API
  module V1
    module FRONT
      class Regions < Grape::API
        include Grape::Kaminari

        resource :front do

          desc 'Front - List Regions'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get "/regions" do
            searched = Region.search(params[:search]).order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::ADMIN::DataManager::Region::Index, count: Region.count, searched: searched.count
          end

          desc 'Front - List All Regions'
          get "/regions/all" do
            searched = Region.order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::ADMIN::DataManager::Region::Index, count: Region.count, searched: searched.count
          end

          desc 'Front - Specific Region'
          # id                     (integer)
          get "/regions/:id" do
            region = Region.find(params[:id])
            present region, with: Entities::V1::ADMIN::DataManager::Region::RegionInfo
          end
          
        end
      end
    end
  end
end
