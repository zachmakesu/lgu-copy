module API
  module V1
    module FRONT
      class Indicators < Grape::API
        include Grape::Kaminari

        resource :front do
          desc 'Front - List Indicators'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get "/indicators" do
            searched = IndicatorDatum.includes(:key_result_area).search(params[:search])
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::Index, count: IndicatorDatum.count, searched: searched.count
          end

          desc 'Front - List All Indicators'
          get "/indicators/all" do
            searched = IndicatorDatum.includes(:key_result_area).order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::Index, count: IndicatorDatum.count, searched: searched.count
          end


          desc 'Front - Specific Indicator'
          # id                     (integer)
          get "/indicators/:id" do
            indicator_datum = IndicatorDatum.includes(:key_result_area).find(params[:id])
            present indicator_datum, with: Entities::V1::ADMIN::DataManager::IndicatorDatum::IndicatorDatumInfo
          end
          
        end
      end
    end
  end
end
