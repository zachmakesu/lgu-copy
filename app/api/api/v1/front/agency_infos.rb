module API
  module V1
    module FRONT
      class AgencyInfos < Grape::API
        resource :front do
          desc "Front - Get Agency Info"
          get "/agency_info" do
            present AgencyInfo.first, with: Entities::V1::ADMIN::Settings::AgencyInfo::Index
          end
        end
      end
    end
  end
end
