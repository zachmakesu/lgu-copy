module API
  module V1
    module FRONT
      class LguNames < Grape::API
        include Grape::Kaminari

        resource :front do

          desc 'Front - List LGU Names'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get "/lgu_names" do
            searched = LguName.includes(:lgu_type, :region, :province).search(params[:search]).order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::FRONT::LguName::Index, count: LguName.count, searched: searched.count
          end

          desc 'Front - List All LGU Names'
          get "/lgu_names/all" do
            all_data = { data: LguName.includes(:lgu_type, :region, :province).order(name: :asc) }
            present all_data, with: Entities::V1::FRONT::LguName::IndexWithOutOverallRatings, count: LguName.count
          end

          desc 'Front - List All LGU Names with overall_ratings'
          get "/lgu_names/all_with_overall_ratings" do
            all_data = { data: LguName.includes(:lgu_type, :region, :province).order(name: :asc) }
            present all_data, with: Entities::V1::FRONT::LguName::Index, count: LguName.count
          end

          desc "Front - Compare LGU Names"
          # lgu_name_ids (string)
          get "/lgu_names/compare" do
            if params[:lgu_name_ids].present? 
              lgu_name_ids = params[:lgu_name_ids].split('^')
              ids = lgu_name_ids.uniq.map {|i| i.to_i }.uniq
              if !ids.include?(0) || ids.blank?
                lgu_names = { data: LguName.includes(data_containers: [{ indicator_datum: [:key_result_area] }, :raw_data]).where(id: ids) }
                overall_rating_ids = IndicatorDatum.overall_rating.map(&:id)
                present lgu_names, with: Entities::V1::ADMIN::LguNameDataManager::LguNameDatum::Compare, overall_rating_ids: overall_rating_ids

              else
                error!({messages: "Invalid Paramater"},400)
              end
            else
              error!({messages: "Invalid Paramater"},400)
            end
          end

          desc 'Front - Specific LGU Name basic info'
          # id                     (integer)
          get "/lgu_names/:id" do
            lgu_name = LguName.includes(:lgu_type, :region, :province).find(params[:id])
            present lgu_name, with: Entities::V1::FRONT::LguName::LguNameInfo
          end

          desc "Front - Specific LGU Name with all indicators"
          # id                     (integer)
          get "/lgu_names/:id/indicators" do
            lgu_name = { data: LguName.includes(data_containers: [{ indicator_datum: [:key_result_area] }, :raw_data]).find(params[:id]) } 
            
            overall_rating_ids = IndicatorDatum.overall_rating.map(&:id)
            overall_ratings = lgu_name[:data].data_containers.where(indicator_datum_id: overall_rating_ids)

            present lgu_name, with: Entities::V1::ADMIN::LguNameDataManager::LguNameDatum::Index, overall_ratings: overall_ratings
          end

          desc "Front - Specific LGU Name with Specific indicator"
          # id                     (integer)
          # indicator_id           (integer)
          get "/lgu_names/:id/indicators/:indicator_id" do
            lgu_name = LguName.includes(data_containers: [{ indicator_datum: [:key_result_area] }, :raw_data]).find(params[:id]) 
            data_container = lgu_name.data_containers.where(indicator_datum_id: params[:indicator_id])
            overall_rating_ids = IndicatorDatum.overall_rating.map(&:id)
            overall_ratings = lgu_name.data_containers.where(indicator_datum_id: overall_rating_ids)

            present lgu_name, with: Entities::V1::ADMIN::LguNameDataManager::LguNameDatum::LguNameIndicatorInfo, data_container: data_container, overall_ratings: overall_ratings
          end

          desc "Front - Specific LGU Name to PDF"
         
          get "/lgu_names/:lgu_code/to_pdf/:year" do
            year = params[:year]
            validate_year_format!(year)
            lgu_name = LguName.includes(data_containers: [:indicator_datum, :raw_data]).find_by!(lgu_code: params[:lgu_code]) 
            present lgu_name, with: Entities::V1::FRONT::LguName::LguNameToPdf, year: Date.strptime("#{year.to_i}", "%Y").year
          end

        end
      end
    end
  end
end
