module API
  module V1
    module FRONT
      class LguTypes < Grape::API
        include Grape::Kaminari

        resource :front do

          desc 'Front - List LguTypes'
          paginate per_page: 10, offset: 0
          # params on pagination ?page=1&per_page=10&search=text
          get "/lgu_types" do
            searched = LguType.search(params[:search]).order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::ADMIN::DataManager::LguType::Index, count: LguType.count, searched: searched.count
          end

          desc 'Front - List All LguTypes'
          get "/lgu_types/all" do
            searched = LguType.order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::ADMIN::DataManager::LguType::Index, count: LguType.count, searched: searched.count
          end

          desc 'Front - Specific LguType'
          # id                     (integer)
          get "/lgu_types/:id" do
            lgu_type = LguType.find(params[:id])
            present lgu_type, with: Entities::V1::ADMIN::DataManager::LguType::LguTypeInfo
          end
          
        end
      end
    end
  end
end
