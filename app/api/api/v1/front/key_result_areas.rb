module API
  module V1
    module FRONT
      class KeyResultAreas < Grape::API
        include Grape::Kaminari

        resource :front do

          desc 'Front - List Key result areas'
          paginate per_page: 10, offset: 0
          get "/key_result_areas" do
            searched = KeyResultArea.order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::ADMIN::DataManager::KeyResultArea::IndexFront, count: KeyResultArea.count, searched: searched.count
          end

          desc 'Front - List All Key result areas'
          get "/key_result_areas/all" do
            searched = KeyResultArea.order(name: :asc)
            all_data = { data: searched }
            present all_data, with: Entities::V1::ADMIN::DataManager::KeyResultArea::IndexFront, count: KeyResultArea.count, searched: searched.count
          end

          desc 'Front - Specific KRA'
          # id                     (integer)
          get "/key_result_areas/:id" do
            key_result_area = KeyResultArea.find(params[:id])
            present key_result_area, with: Entities::V1::ADMIN::DataManager::KeyResultArea::KeyResultAreaInfo
          end

          desc 'Front - Specific KRA indicators'
          # id                     (integer)
          get "/key_result_areas/:id/indicators" do
            key_result_area = KeyResultArea.find(params[:id])
            searched = key_result_area.indicator_data.order(name: :asc)
            all_data = { data: paginate(searched) }
            present all_data, with: Entities::V1::ADMIN::DataManager::Indicator::IndexFront, count: Indicator.count, searched: searched.count
          end

        end
      end
    end
  end
end
