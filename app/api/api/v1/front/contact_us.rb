module API
  module V1
    module FRONT
      class ContactUs < Grape::API
        resource :front do
          desc "Front - Send form on contact us"
          # name (string)
          # email (string)
          # message (string)
          post "/contact_us" do
          handler = ContactUsHandler.new(params).send
            if handler.response[:success]
              {messages: handler.response[:details]}
            else
              error!({messages: handler.response[:details]},400)
            end
          end
        end
      end
    end
  end
end
