module API
  module V1
    class Base < Grape::API
      version 'v1'
      format :json

      rescue_from :all, backtrace: true do |e|
        ExceptionNotifier.notify_exception(e)
        Rails.logger.debug e if Rails.env.development?
        error!({ error: 'Internal server error', backtrace: e.backtrace }, 500 )
      end
      rescue_from ActiveRecord::RecordNotFound do |e|
        rack_response('{ "status": 404, "message": "Requested resource not found" }', 404)
      end

      rescue_from ActiveRecord::InvalidForeignKey do |e|
        rack_response('{ "status": 404, "message": "Parent record not found" }', 404)
      end

      rescue_from ActiveRecord::RecordInvalid do |e|
        rack_response('{ "status": 404, "message": "Invalid create or update request" }', 404)
      end

      rescue_from ArgumentError do |e|
        rack_response('{ "status": 404, "message": "Invalid values on optional fields " }', 422)
      end

      rescue_from ActionController::ParameterMissing do |e|
        rack_response('{ "status": 404, "message": "Invalid required parameters" }', 422)
      end

      get do
        {
          version: version
        }
      end
      
      #admin
      mount API::V1::ADMIN::Base

      #front
      mount API::V1::FRONT::AuthRequiredFront
    end
  end
end