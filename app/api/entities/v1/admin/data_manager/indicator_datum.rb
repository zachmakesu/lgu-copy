module Entities
  module V1
    module ADMIN
      module DataManager
        module IndicatorDatum

          class BenchMarkInfo < Grape::Entity
            expose :income_class_id, :income_class, :value

            private
            def income_class_id
              object.id
            end
          end

          class IndicatorDatumListing < Grape::Entity
            expose  :id, :name, :key_result_area_id, :key_result_area_name, :formula
            expose  :benchmarks, using: BenchMarkInfo

            private
            def benchmarks
              object.bench_marks
            end
            def key_result_area_name
              object.key_result_area.present? ? object.key_result_area.name : nil
            end
          end

          class Index < Grape::Entity
            expose :data, using: IndicatorDatumListing
            expose :total_rows, :total_results, :total_searched

            private
            def data
              object[:data]
            end

            def total_rows
              options[:count]
            end

            def total_results
              object[:data].count
            end

            def total_searched
              options[:searched]
            end

          end

          class IndicatorDatumInfo < Grape::Entity
            expose :data, using: IndicatorDatumListing
            private
            def data
              object
            end
          end

        end
      end
    end
  end
end