module Entities
  module V1
    module ADMIN
      module DataManager
        module LguCoord

          class LguCoordListing < Grape::Entity
            expose  :id, :total_process, :total_processed
          end

          class Index < Grape::Entity
            expose :data, using: LguCoordListing
          
            private
            def data
              object[:data]
            end

          end

        end
      end
    end
  end
end