module Entities
  module V1
    module ADMIN
      module DataManager
        module KeyResultArea

          class KeyResultAreaListing < Grape::Entity
            expose  :id, :name
          end


          class KeyResultAreaListingFront < Grape::Entity
            expose  :id, :name
            
            private
            def key_result_area
              object.key_result_area.try(:name)
            end
          end

          class Index < Grape::Entity
            expose :data, using: KeyResultAreaListing
            expose :total_rows, :total_results, :total_searched
            
            private
            def data
              object[:data]
            end

            def total_rows
              options[:count]
            end

            def total_results
              object[:data].count
            end

            def total_searched
              options[:searched]
            end
          end

          class KeyResultAreaInfo < Grape::Entity
            expose :data, using: KeyResultAreaListing
            private
            def data
              object
            end
          end

          class IndexFront < Grape::Entity
            expose :data, using: KeyResultAreaListingFront

            private
            def data
              object[:data]
            end
          end

        end
      end
    end
  end
end