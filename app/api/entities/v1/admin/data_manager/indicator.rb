module Entities
  module V1
    module ADMIN
      module DataManager
        module Indicator

          class IndicatorListing < Grape::Entity
            expose  :id, :key_result_area_id, :key_result_area, :name
            expose  :list_of_data, using: Entities::V1::ADMIN::DataManager::ListOfDatum::ListOfDatumListing

            private
            def key_result_area
              object.key_result_area.try(:name)
            end

          end

          class IndicatorListingFront < Grape::Entity
            expose  :id, :key_result_area_id, :key_result_area, :name
            
            private
            def key_result_area
              object.key_result_area.try(:name)
            end
          end

          class Index < Grape::Entity
            expose :data, using: IndicatorListing
            expose :total_rows, :total_results, :total_searched

            private
            def data
              object[:data]
            end

            def total_rows
              options[:count]
            end

            def total_results
              object[:data].count
            end

            def total_searched
              options[:searched]
            end

          end

          class IndicatorInfo < Grape::Entity
            expose :data, using: IndicatorListing
            private
            def data
              object
            end
          end

          class IndexFront < Grape::Entity
            expose :data, using: IndicatorListingFront

            private
            def data
              object[:data]
            end
          end

        end
      end
    end
  end
end