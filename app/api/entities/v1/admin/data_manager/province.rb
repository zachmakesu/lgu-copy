module Entities
  module V1
    module ADMIN
      module DataManager
        module Province

          class ProvinceListing < Grape::Entity
            expose  :id, :name, :geocode
          end

          class Index < Grape::Entity
            expose :data, using: ProvinceListing
            expose :total_rows, :total_results, :total_searched

            private
            def data
              object[:data]
            end

            def total_rows
              options[:count]
            end

            def total_results
              object[:data].count
            end

            def total_searched
              options[:searched]
            end
          end

          class ProvinceInfo < Grape::Entity
            expose :data, using: ProvinceListing
            private
            def data
              object
            end
          end

        end
      end
    end
  end
end