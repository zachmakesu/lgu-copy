module Entities
  module V1
    module ADMIN
      module DataManager
        module CsvFile

          class CsvFileListing < Grape::Entity
            expose  :id, :total_rows, :total_processed_rows, :csv_file_name
          end

          class Index < Grape::Entity
            expose :data, using: CsvFileListing
          
            private
            def data
              object[:data]
            end

          end

        end
      end
    end
  end
end