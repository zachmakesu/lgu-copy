module Entities
  module V1
    module ADMIN
      module DataManager
        module ListOfDatum

          class ListOfDatumListing < Grape::Entity
            expose  :id, :indicator_datum_id, :indicator_datum

            private
            def indicator_datum_id
              object.indicator_datum.try(:id)
            end

            def indicator_datum
              object.indicator_datum.try(:name)
            end

          end

          class Index < Grape::Entity
            expose :data, using: ListOfDatumListing
            private
            def data
              object[:data]
            end
          end

          class ListOfDatumInfo < Grape::Entity
            expose :data, using: ListOfDatumListing
            private
            def data
              object
            end
          end

        end
      end
    end
  end
end