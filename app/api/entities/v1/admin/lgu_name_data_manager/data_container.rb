module Entities
  module V1
    module ADMIN
      module LguNameDataManager
        module DataContainer

          class DataContainerListing < Grape::Entity
            expose :container_id, :indicator_id, :indicator_name, :score_visualization, :benchmark_visualization, :comparison_visualization 
            expose :key_result_area_id, :key_result_area_name
            expose :raw_data, using: Entities::V1::ADMIN::LguNameDataManager::RawDatum::RawDatumListing

            private
              def container_id
                object.id
              end

              def indicator_id
                object.indicator_datum.try(:id)
              end

              def indicator_name
                object.indicator_datum.try(:name)
              end

              def key_result_area_id
                object.indicator_datum.key_result_area.try(:id)
              end

              def key_result_area_name
                object.indicator_datum.key_result_area.try(:name)
              end
          end

          class Index < Grape::Entity
            expose :data, using: DataContainerListing
            private
            def data
              object[:data]
            end
          end

          class DataContainerInfo < Grape::Entity
            expose :data, using: DataContainerListing
            private
            def data
              object
            end
          end

        end
      end
    end
  end
end