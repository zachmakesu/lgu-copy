module Entities
  module V1
    module ADMIN
      module LguNameDataManager
        module LguNameDatum
          class RawData < Grape::Entity
            expose  :id, :year, :datum
          end

          class OverAllRatings < Grape::Entity
            expose  :name
            expose  :raw_data, using: RawData
            private
            def name
              object.indicator_datum.try(:name)
            end
          end

          class LguNameDatumListing < Grape::Entity
            expose  :id, :name
            expose  :overall_ratings, using: OverAllRatings
            expose  :data_containers, using: Entities::V1::ADMIN::LguNameDataManager::DataContainer::DataContainerListing

            private
            def overall_ratings
              options[:overall_ratings]
            end

          end

          class Index < Grape::Entity
            expose  :data, using: LguNameDatumListing
            private
            def data
              object[:data]
            end
          end

          class LguNameDatumInfo < Grape::Entity
            expose  :data, using: LguNameDatumListing
            private
            def data
              object
            end
          end

          class LguNameIndicatorInfo < Grape::Entity
            expose  :data do
              expose  :id, :name
              expose  :overall_ratings, using: OverAllRatings
              expose  :data_container, using: Entities::V1::ADMIN::LguNameDataManager::DataContainer::DataContainerListing
            end

            private
            def data_container
              options[:data_container]
            end

            def overall_ratings
              options[:overall_ratings]
            end

          end

          class CompareInfo < Grape::Entity
            expose  :id, :name
            expose  :overall_ratings, using: OverAllRatings
            expose  :data_containers, using: Entities::V1::ADMIN::LguNameDataManager::DataContainer::DataContainerListing

            private
            def overall_ratings
              object.data_containers.where(indicator_datum_id: options[:overall_rating_ids])
            end
          end

          class Compare < Grape::Entity
            expose  :data, using: CompareInfo

            private
            def data
              object[:data]
            end
          end


        end
      end
    end
  end
end
