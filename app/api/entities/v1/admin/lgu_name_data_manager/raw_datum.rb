module Entities
  module V1
    module ADMIN
      module LguNameDataManager
        module RawDatum

          class RawDatumListing < Grape::Entity
            expose  :id, :year, :datum
          end

          class Index < Grape::Entity
            expose :data, using: RawDatumListing
            private
            def data
              object[:data]
            end
          end

          class RawDatumInfo < Grape::Entity
            expose :data, using: RawDatumListing
            private
            def data
              object
            end
          end

        end
      end
    end
  end
end