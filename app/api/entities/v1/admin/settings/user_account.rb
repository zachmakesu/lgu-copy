module Entities
  module V1
    module ADMIN
      module Settings
        module UserAccount
          
          class UserAccountList < Grape::Entity
            expose :employee_id, :email, :first_name, :last_name, :role
            expose :profile_image, format_with: :original_photo_url
            expose :status

            private
            def profile_image
              object.avatar.present? ? object.avatar : nil
            end

            def status
              object.deleted_at.present? ? "deactivated" : "activated"
            end
          end

          class UserInfo < Grape::Entity
            expose :data, using: UserAccountList
            private
            def data
              object.present? ? object : nil
            end
          end

          class Index < Grape::Entity
            expose :data, using: UserAccountList
            expose :total_rows, :total_results, :total_searched

            private
            def data
              object[:data]
            end

            def total_rows
              options[:count]
            end

            def total_results
              object[:data].count
            end

            def total_searched
              options[:searched]
            end

          end

          class UserInfoLogin < Grape::Entity
            expose :data do 
              expose :access_token, :employee_id, :email, :first_name, :last_name, :role
              expose :profile_image, format_with: :original_photo_url
              expose :status
            end            

            private
            def profile_image
              object.avatar
            end

            def access_token
              options[:access_token]
            end

            def status
              object.deleted_at.present? ? "deactivated" : "activated"
            end
          end

        end
      end
    end
  end
end