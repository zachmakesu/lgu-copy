module Entities
  module V1
    module ADMIN
      module Settings
        module AgencyInfo

          class Index < Grape::Entity
            expose :data do
              expose  :telephone_number,
                      :mobile_number,
                      :email,
                      :address,
                      :about_us
            end

            private
            def data
              object
            end

          end
        end
      end
    end
  end
end