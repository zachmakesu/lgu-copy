module Entities
  module V1
    module FRONT
      module LguName
        class LguNameListing < Grape::Entity
          expose  :id, :region, :province, :lgu_type, :name, :lgu_code, :income_class
          expose :lgu_seal, :lgu_background, format_with: :original_photo_url
          expose  :coords do
              expose :latitude, :longitude
          end
          expose :overall_ratings, using: Entities::V1::ADMIN::LguNameDataManager::LguNameDatum::OverAllRatings

          private
            def region
              object.region.try(:name)
            end

            def province
              object.province.try(:name)
            end

            def lgu_type
              object.lgu_type.try(:name)
            end

            def lgu_seal
              object.seal
            end

            def lgu_background
              object.background
            end
        end

        class Index < Grape::Entity
          expose :data, using: LguNameListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
              object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class LguNameListingWithOutOverallRatings < Grape::Entity
          expose  :id, :region, :province, :lgu_type, :name, :lgu_code, :income_class
          expose :lgu_seal, :lgu_background, format_with: :original_photo_url
          expose  :coords do
              expose :latitude, :longitude
          end

          private
            def region
              object.region.try(:name)
            end

            def province
              object.province.try(:name)
            end

            def lgu_type
              object.lgu_type.try(:name)
            end

            def lgu_seal
              object.seal
            end

            def lgu_background
              object.background
            end
        end

        class IndexWithOutOverallRatings < Grape::Entity
          expose :data, using: LguNameListingWithOutOverallRatings
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
              object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class LguNameInfo < Grape::Entity
          expose :data, using: LguNameListing
          private
          def data
            object
          end
        end

        class RawData < Grape::Entity
          expose :year, :datum
        end

        class ListOfData < Grape::Entity
          expose :indicator_id, :indicator, :indicator_datum_id, :indicator_datum, :container_id, :benchmark_visualization, :comparison_visualization, :score_visualization
          expose :raw_data, using: RawData

          private

          def container_id
            object.id
          end

          def indicator_id
            options[:indicator].try(:id)
          end

          def indicator
            options[:indicator].try(:name)
          end

          def indicator_datum_id
            object.indicator_datum.try(:id)
          end

          def indicator_datum
            object.indicator_datum.try(:name)
          end
          
        end

        class Indicator < Grape::Entity
          expose :id, :name
          expose :list_of_data, using: ListOfData

          private
          def list_of_data
            options[:data_containers]
          end
        end

      
        class LguNameIndicatorData < Grape::Entity
          expose :data do
            expose :id, :name
            expose  :coords do
              expose :latitude, :longitude
            end
            expose :indicator, using: Indicator
            
          end

          private
          def data
            object
          end

          def indicator
            options[:indicator]
          end
        end

        class LguNameToPdf < Grape::Entity
          expose :data do 
            expose  :id, :region, :province, :lgu_type, :name, :lgu_code, :income_class
            expose :lgu_seal, :lgu_background, format_with: :original_photo_url
            expose  :coords do
                expose :latitude, :longitude
            end
            expose :year
            expose :pdf_data
          end

          private
            def region
              object.region.try(:name)
            end

            def province
              object.province.try(:name)
            end

            def lgu_type
              object.lgu_type.try(:name)
            end

            def lgu_seal
              object.seal
            end

            def lgu_background
              object.background
            end

            def year
              options[:year]
            end

            def pdf_data
              object.pdf_data(options[:year])
            end
        end

      end
    end
  end
end