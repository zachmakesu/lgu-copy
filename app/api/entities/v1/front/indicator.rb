module Entities
  module V1
    module FRONT
      module Indicator
        class IndicatorListing < Grape::Entity
          expose  :id, :indicator_name, :key_result_area, :kra_id

          private
          def indicator_name
            object.name
          end

          def key_result_area
            object.key_result_area.present? ? object.key_result_area.name : nil
          end

          def kra_id
            object.key_result_area.present? ? object.key_result_area.id : nil
          end

        end

        class Index < Grape::Entity
          expose :data, using: IndicatorListing
          expose :total_rows, :total_results, :total_searched

          private
          def data
            object[:data]
          end

          def total_rows
            options[:count]
          end

          def total_results
              object[:data].count
          end

          def total_searched
            options[:searched]
          end
        end

        class IndicatorInfo < Grape::Entity
          expose :data, using: IndicatorListing
          private
          def data
            object
          end
        end

      end
    end
  end
end