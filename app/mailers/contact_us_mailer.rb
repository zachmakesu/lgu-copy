class ContactUsMailer < ApplicationMailer
  default from: "noreply@gorated.com"
  def send_message(contact_options)
    @params = contact_options
    mail(to: 'caasi.mirabueno@gmail.com', subject: 'Message from Contact Us')
  end
end
