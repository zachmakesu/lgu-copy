class CsvImportWorker
  include Sidekiq::Worker

  def perform(file_id)
    while csv_still_blank?(file_id) do
       #do nothing
    end

    Encoding.default_external = "iso-8859-1"
    
    @file = CsvFile.find_by(id: file_id)
    @csv_file = Hash.new
    @csv_file[:tempfile] = File.open(File.join(@file.csv.path))

    require 'csv' 
  
    CSV.foreach(@csv_file[:tempfile], headers: true) do |row|
      year = @file.csv_file_name[0..3].to_i
      hash_data = row.to_hash

      arr_data = hash_data.to_a
      LguDataWorker.perform_async(arr_data, hash_data, year)

      @file.update(total_processed_rows: $.)
      @file.update(finished: true) if @file.total_processed_rows == @file.total_rows
    end
    
  end

  def csv_still_blank?(file_id)
    CsvFile.find_by(id: file_id).blank?
  end
end