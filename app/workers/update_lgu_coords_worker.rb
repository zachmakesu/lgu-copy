class UpdateLguCoordsWorker
  include Sidekiq::Worker

  def perform(background_process_id,lgu_name_id, count)
    @lgu_name = LguName.find(lgu_name_id)
    @background_process = BackgroundProcess.find(background_process_id)
    @lgu_name.update_latlong
    count = ( count >= (@background_process.total_process - 50)) ? @background_process.total_process : count
    @background_process.update(total_processed: count)
  end

end