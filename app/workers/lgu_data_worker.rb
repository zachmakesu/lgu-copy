class LguDataWorker
  include Sidekiq::Worker

  def perform(arr_data, hash_data, year)
    @year = year
    Encoding.default_external = "iso-8859-1"

    name = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
    lgu_type  = LguType.find_or_create_by(name: name) { |data| data.name = name }
    
    name = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
    region    = Region.find_or_create_by(name: name) { |data| data.name = name }

    name = arr_data[2][1].present? ? arr_data[2][1].rstrip.lstrip : nil
    geocode = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
    province  = Province.find_or_create_by(name: name, geocode: geocode) do |data| 
      data.name = name 
      data.geocode = geocode
    end

    name = arr_data[0][1].present? ? arr_data[0][1].rstrip.lstrip : nil
    @lgu_type_id   = LguType.find_by(name: name).try(:id)

    name = arr_data[1][1].present? ? arr_data[1][1].rstrip.lstrip : nil
    @region_id     = Region.find_by(name: name).try(:id)

    geocode = arr_data[3][1].present? ? arr_data[3][1].rstrip.lstrip : nil
    @province_id   = Province.find_by(geocode: geocode).try(:id)

    name = arr_data[4][1].present? ? arr_data[4][1].rstrip.lstrip : nil
    lgu_code = arr_data[5][1].present? ? arr_data[5][1].rstrip.lstrip : nil
    income_class = arr_data[6][1].present? ? arr_data[6][1].rstrip.lstrip : nil

    @lgu_name = LguName.find_or_create_by(lgu_code: lgu_code) do |data| 
      data.name = name
      data.lgu_type_id = @lgu_type_id
      data.region_id = @region_id
      data.province_id = @province_id
      data.lgu_code = lgu_code
      data.income_class = income_class
    end

    @lgu_name.update_attributes(name: name, lgu_type_id: @lgu_type_id, region_id: @region_id, province_id: @province_id, income_class: income_class ) if @lgu_name.present?
    
    counter = 0
    hash_data.each do |v|
      if counter < 7  
        # do nothing
      else
        # #puts "#{v[0]} - #{v[1]}"
        name = v[0].present? ? v[0].rstrip.lstrip : nil
        indicator_datum = IndicatorDatum.find_or_create_by(name: name) { |data| data.name = name }

        data_container = @lgu_name.data_containers.find_or_create_by(indicator_datum: indicator_datum) do |data|
          data.indicator_datum = indicator_datum
        end
        
        raw_datum = data_container.raw_data.find_or_create_by(year: @year) do |data|
          data.year = @year
          datum = v[1].present? ? v[1].rstrip.lstrip : nil
          data.datum = datum
        end
        
        if raw_datum.present?
          datum = v[1].present? ? v[1].rstrip.lstrip : raw_datum.datum
          raw_datum.update_attributes(datum: datum)
        end

      end

      counter += 1
    end
  end
end