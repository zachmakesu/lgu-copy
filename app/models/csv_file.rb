# == Schema Information
#
# Table name: csv_files
#
#  created_at           :datetime         not null
#  csv_content_type     :string
#  csv_file_name        :string
#  csv_file_size        :integer
#  csv_updated_at       :datetime
#  csvable_id           :integer
#  csvable_type         :string
#  finished             :boolean          default(FALSE)
#  id                   :integer          not null, primary key
#  total_processed_rows :integer          default(0)
#  total_rows           :integer          default(0)
#  updated_at           :datetime         not null
#

class CsvFile < ActiveRecord::Base
  #1st assotiations
  belongs_to :csvable, polymorphic: true
  
  #2nd scopes
  scope :not_finished, -> { where("total_processed_rows < total_rows and finished = false ") }

  #3rd enums and others

  #4th callbacks

  #5th validations
  has_attached_file :csv
  validates_attachment_content_type :csv, :content_type => ['text/csv','text/comma-separated-values','text/csv','application/csv','text/anytext','text/plain'], :message => 'not allowed.'

  validates :csv, presence: {:on => :create} 

end
