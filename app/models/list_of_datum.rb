# == Schema Information
#
# Table name: list_of_data
#
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  indicator_datum_id :integer
#  indicator_id       :integer
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_list_of_data_on_indicator_datum_id  (indicator_datum_id)
#  index_list_of_data_on_indicator_id        (indicator_id)
#
# Foreign Keys
#
#  fk_rails_79aa5117f8  (indicator_id => indicators.id)
#  fk_rails_e3843d66c7  (indicator_datum_id => indicator_data.id)
#

class ListOfDatum < ActiveRecord::Base
  #1st assotiations
  belongs_to :indicator, class_name: "Indicator", foreign_key: "indicator_id"
  belongs_to :indicator_datum, class_name: "IndicatorDatum", foreign_key: "indicator_datum_id"

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations

  validates :indicator_id, :indicator_datum_id, presence: true
  validates :indicator_datum_id, uniqueness: { scope: :indicator_id }

end
