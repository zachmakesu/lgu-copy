# == Schema Information
#
# Table name: lgu_names
#
#  created_at   :datetime         not null
#  id           :integer          not null, primary key
#  income_class :string
#  latitude     :float
#  lgu_code     :integer
#  lgu_type_id  :integer
#  longitude    :float
#  name         :string           not null
#  province_id  :integer
#  region_id    :integer
#  updated_at   :datetime         not null
#

class LguName < ActiveRecord::Base
  #1st assotiations
  GOOGLEMAPAPIKEY = "AIzaSyCf52A8MY6dw41eOMjPnItKGzFRCkLIwec" 

  belongs_to :lgu_type, class_name: "LguType", foreign_key: "lgu_type_id"
  belongs_to :province, class_name: "Province", foreign_key: "province_id"
  belongs_to :region, class_name: "Region", foreign_key: "region_id"

  has_many :data_containers, dependent: :destroy
  has_many :photos, as: :imageable, dependent: :destroy
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :name, :lgu_code, presence: true
  validates :lgu_code, uniqueness: true

  def self.search(search)
    where('name iLIKE :search', search: "%#{search}%").order(name: :asc)  
  end

  def seal
    photo = self.photos.seal.order(id: :asc).last
    photo ? photo : nil
  end

  def background
    photo = self.photos.background.order(id: :asc).last
    photo ? photo : nil
  end


  def update_latlong
    address = if self.province
                "#{self.name},#{self.province.name}"
              else
                "#{self.name}" 
              end
    source = "https://maps.googleapis.com/maps/api/geocode/json?address=#{address}&key=#{GOOGLEMAPAPIKEY}"
    uri = URI(URI.escape(source))
    response = Net::HTTP.get(uri)
    json = JSON.parse(response)

    if json["results"].present?
      self.latitude  = json["results"][0]["geometry"]["location"]["lat"]
      self.longitude = json["results"][0]["geometry"]["location"]["lng"]
      self.save
    end

  end

  def self.csv_header
    #Using ruby's built-in CSV::Row class
    #true - means its a header
    CSV::Row.new([:lgu_type, :region, :province, :name, :lgu_code], ['LGU Type','REGION','PROVINCE','LGU NAME', 'LGU CODE'], true)
  end

  def to_csv_row
    lgu_type_field = lgu_type.present? ? lgu_type.name : nil
    region_field = region.present? ? region.name : nil
    province_field = province.present? ? province.name : nil
    name_field = name.present? ? name : nil
    lgu_code_field = lgu_code.present? ? lgu_code : nil
    
    fields = [lgu_type: lgu_type_field, region: region_field, province: province_field, name: name_field, lgu_code: lgu_code_field]
    field_data = [lgu_type_field, region_field, province_field, name_field, lgu_code_field]
    CSV::Row.new(fields, field_data)
  end

  def self.find_in_batches(filters, batch_size, &block)
    #find_each will batch the results instead of getting all in one go
    where(filters).find_each(batch_size: batch_size) do |score|
      yield score
    end
  end

  def pdf_data(year)
    pdf_data = Hash.new
    self.data_containers.map do |d|
      obj = d.raw_data.where(year: year).first
      datum = obj.present? ? obj.datum : ""
       pdf_data[d.indicator_datum.name] = datum
    end

    pdf_data
  end

  def overall_ratings
    overall_rating_ids = IndicatorDatum.overall_rating.map(&:id)
    overall_ratings = self.data_containers.where(indicator_datum_id: overall_rating_ids).order(id: :desc)
  end

   
end
