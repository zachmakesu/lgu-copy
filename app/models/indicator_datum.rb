# == Schema Information
#
# Table name: indicator_data
#
#  created_at         :datetime         not null
#  formula            :string
#  id                 :integer          not null, primary key
#  key_result_area_id :integer
#  name               :string           not null
#  updated_at         :datetime         not null
#

class IndicatorDatum < ActiveRecord::Base
  #1st assotiations
  belongs_to :key_result_area, class_name: "KeyResultArea", foreign_key: "key_result_area_id"
  has_many :data_containers, dependent: :destroy
  has_many :bench_marks, dependent: :destroy

  #2nd scopes
  scope :overall_rating, -> { where('name iLIKE  ?', "%Overall%")  }
  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :name, presence: true
  validates :name, uniqueness: true

  def self.search(search)
    where('name iLIKE :search', search: "%#{search}%").order(name: :asc)  
  end

end
