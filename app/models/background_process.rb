# == Schema Information
#
# Table name: background_processes
#
#  created_at       :datetime         not null
#  id               :integer          not null, primary key
#  processable_id   :integer
#  processable_type :string
#  total_process    :integer          default(0)
#  total_processed  :integer          default(0)
#  updated_at       :datetime         not null
#

class BackgroundProcess < ActiveRecord::Base
  #1st assotiations
  belongs_to :processable, polymorphic: true
  
  #2nd scopes
  scope :not_finished, -> { where("total_processed < total_process") }

  #3rd enums and others
  
  #4th callbacks

  #5th validations
end
