# == Schema Information
#
# Table name: bench_marks
#
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  income_class       :string           not null
#  indicator_datum_id :integer
#  updated_at         :datetime         not null
#  value              :string
#
# Indexes
#
#  index_bench_marks_on_indicator_datum_id  (indicator_datum_id)
#
# Foreign Keys
#
#  fk_rails_acaa00d99a  (indicator_datum_id => indicator_data.id)
#

class BenchMark < ActiveRecord::Base
  #1st assotiations
  belongs_to :indicator_datum, class_name: "IndicatorDatum", foreign_key: "indicator_datum_id"
  
  #2nd scopes
  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :income_class, presence: true
  validates :income_class, uniqueness: { scope: :indicator_datum_id }
end
