# == Schema Information
#
# Table name: agency_infos
#
#  about_us         :text
#  address          :text
#  created_at       :datetime         not null
#  email            :string
#  id               :integer          not null, primary key
#  mobile_number    :string
#  telephone_number :string
#  updated_at       :datetime         not null
#

class AgencyInfo < ActiveRecord::Base
end
