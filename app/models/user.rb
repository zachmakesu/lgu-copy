# == Schema Information
#
# Table name: users
#
#  created_at             :datetime         not null
#  current_sign_in_at     :datetime
#  current_sign_in_ip     :inet
#  deleted_at             :string
#  email                  :string           default(""), not null
#  employee_id            :string           not null
#  encrypted_password     :string           default(""), not null
#  failed_attempts        :integer          default(0), not null
#  first_name             :string
#  id                     :integer          not null, primary key
#  last_name              :string
#  last_sign_in_at        :datetime
#  last_sign_in_ip        :inet
#  locked_at              :datetime
#  remember_created_at    :datetime
#  reset_password_sent_at :datetime
#  reset_password_token   :string
#  role                   :integer          default(0), not null
#  sign_in_count          :integer          default(0), not null
#  unlock_token           :string
#  updated_at             :datetime         not null
#
# Indexes
#
#  index_users_on_email                 (email) UNIQUE
#  index_users_on_reset_password_token  (reset_password_token) UNIQUE
#  index_users_on_unlock_token          (unlock_token) UNIQUE
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :lockable

  #1st assotiations
  has_many :api_keys, dependent: :destroy
  has_many :csv_files, as: :csvable, dependent: :destroy
  has_many :photos, as: :imageable, dependent: :destroy
  has_many :background_processes, as: :processable, dependent: :destroy
  
  #2nd scopes
  scope :not_deleted, -> { where(deleted_at: nil) }
  scope :except_self, -> (user) { where.not(id: user.id ) }

  #3rd enums and others
  enum role: { normal: 0, admin: 1 }

  #4th callbacks

  #5th validations
  validates_presence_of :email, :first_name, :last_name, :role
  validates_uniqueness_of :email

  def self.search(search)
    where('first_name iLIKE :search or last_name iLIKE :search or email iLIKE :search', search: "%#{search}%").order(first_name: :asc)  
  end

  def avatar
    photo = self.photos.profile.order(id: :asc).last
    photo ? photo : nil
  end

  def delete_previous_api_keys
    api_keys.first.delete if api_keys.count > 0
  end
end
