# == Schema Information
#
# Table name: raw_data
#
#  created_at        :datetime         not null
#  data_container_id :integer
#  datum             :string
#  id                :integer          not null, primary key
#  updated_at        :datetime         not null
#  year              :integer          not null
#
# Indexes
#
#  index_raw_data_on_data_container_id  (data_container_id)
#
# Foreign Keys
#
#  fk_rails_8097a4bbb6  (data_container_id => data_containers.id)
#

class RawDatum < ActiveRecord::Base
  #1st assotiations
  belongs_to :data_container, class_name: "DataContainer", foreign_key: "data_container_id"

  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :datum, :year, presence: true
  validates :year, uniqueness: { scope: :data_container_id }
end
