# == Schema Information
#
# Table name: provinces
#
#  created_at :datetime         not null
#  geocode    :string
#  id         :integer          not null, primary key
#  name       :string           not null
#  updated_at :datetime         not null
#

class Province < ActiveRecord::Base
  #1st assotiations
  
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :name, presence: true
  validates :geocode, presence: true
  validates :geocode, uniqueness: true

  def self.search(search)
    where('name iLIKE :search or geocode iLIKE :search', search: "%#{search}%").order(name: :asc)  
  end
end
