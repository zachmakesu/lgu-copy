# == Schema Information
#
# Table name: lgu_types
#
#  created_at :datetime         not null
#  id         :integer          not null, primary key
#  name       :string           not null
#  updated_at :datetime         not null
#

class LguType < ActiveRecord::Base
  #1st assotiations
  has_many :regions
  
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
 
  validates :name, presence: true
  validates :name, uniqueness: true

  def self.search(search)
    where('name iLIKE :search', search: "%#{search}%").order(name: :asc)  
  end
end
