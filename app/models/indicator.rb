# == Schema Information
#
# Table name: indicators
#
#  created_at         :datetime         not null
#  id                 :integer          not null, primary key
#  key_result_area_id :integer
#  name               :string           not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_indicators_on_key_result_area_id  (key_result_area_id)
#
# Foreign Keys
#
#  fk_rails_b41ab46c8f  (key_result_area_id => key_result_areas.id)
#

class Indicator < ActiveRecord::Base
  #1st assotiations
  belongs_to :key_result_area, class_name: "KeyResultArea", foreign_key: "key_result_area_id"
  has_many :list_of_data, dependent: :destroy, inverse_of: :indicator
  
  #2nd scopes

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :name, :key_result_area_id, presence: true
  validates :name, uniqueness: { scope: :key_result_area_id }

  def self.search(search)
    where('name iLIKE :search', search: "%#{search}%").order(name: :asc)  
  end
end
