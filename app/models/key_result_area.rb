# == Schema Information
#
# Table name: key_result_areas
#
#  created_at     :datetime         not null
#  id             :integer          not null, primary key
#  indicator_type :integer          default(0), not null
#  name           :string           not null
#  updated_at     :datetime         not null
#

class KeyResultArea < ActiveRecord::Base
  #1st assotiations
  has_many :indicator_data, dependent: :destroy

  #2nd scopes

  #3rd enums and others
  enum indicator_type: { financial: 0, non_financial: 1}

  #4th callbacks

  #5th validations
  validates :name, :indicator_type, presence: true
  validates :name, uniqueness: true

  def self.search(search)
    where('name iLIKE :search', search: "%#{search}%").order(name: :asc)  
  end
end
