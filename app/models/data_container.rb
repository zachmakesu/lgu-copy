# == Schema Information
#
# Table name: data_containers
#
#  benchmark_visualization  :string           default("pie"), not null
#  comparison_visualization :string           default("pie"), not null
#  created_at               :datetime         not null
#  id                       :integer          not null, primary key
#  indicator_datum_id       :integer          not null
#  lgu_name_id              :integer
#  score_visualization      :string           default("pie"), not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_data_containers_on_lgu_name_id  (lgu_name_id)
#
# Foreign Keys
#
#  fk_rails_6e33111f68  (lgu_name_id => lgu_names.id)
#

class DataContainer < ActiveRecord::Base
  #1st assotiations
  belongs_to :lgu_name, class_name: "LguName", foreign_key: "lgu_name_id"
  belongs_to :indicator_datum, class_name: "IndicatorDatum", foreign_key: "indicator_datum_id"

  has_many :raw_data, dependent: :destroy
  #2nd scopes

  scope :search_with_indicator_datum, lambda { |search| 
    joins(:indicator_datum).where('indicator_datum.name iLIKE  ?', "%#{search}%") 
  }

  #3rd enums and others

  #4th callbacks

  #5th validations
  validates :lgu_name_id, :indicator_datum_id, presence: true
  validates :indicator_datum_id, uniqueness: { scope: :lgu_name_id }

end
