IndicatorHandlerError = Class.new(StandardError)

class IndicatorHandler

  attr_accessor :params, :response, :key_result_area

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    return missing_message if params[:indicator][:indicator_datum_id].blank?
    return missing_kra if params[:indicator][:key_result_area_id].blank?
    ActiveRecord::Base.transaction do 
      @indicator = Indicator.create(permited_params)

      if @indicator.errors.blank?
        if params[:indicator][:indicator_datum_id].present?
          @indicator.list_of_data.create(indicator_datum_id: params[:indicator][:indicator_datum_id])
        end
      end

      @errors = @indicator.errors.full_messages.join(', ')
    end
    create_response(@errors)
    self
  end

  def update
    @indicator = Indicator.find_by(id: params[:id])
    return error_message if @indicator.blank? || Indicator.count == 0
    return missing_kra if params[:indicator][:key_result_area_id].blank?
    ActiveRecord::Base.transaction do
      if @indicator.update(permited_params)
        if @indicator.errors.blank?
          if params[:indicator][:indicator_datum_id].present?
            @indicator.list_of_data.create(indicator_datum_id: params[:indicator][:indicator_datum_id])
          end
        end
      else
        @errors = @indicator.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete
    @indicator = Indicator.find_by(id: params[:id])
    return error_message if @indicator.blank? || Indicator.count == 0
    @indicator.destroy

    create_response
    self
  end

  private
  def permited_params
    ActionController::Parameters.new(params).require(:indicator).permit(:name, :key_result_area_id)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end

  def missing_message
    response[:success] = false
    response[:details] = "Missing indicator_datum_id"
    self
  end

  def missing_kra
    response[:success] = false
    response[:details] = "Missing key_result_area_id"
    self
  end
  
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @indicator
    else
      response[:success] = false
      response[:details] = error || @indicator.errors.full_messages.join(', ')
    end
  end

end
