LguNameDataHandlerError = Class.new(StandardError)

class LguNameDataHandler

  attr_accessor :params, :response, :lgu_name, :data_container

  def initialize(params)
    @params   = params
    @lgu_name = LguName.find(params[:id])
    @data_container = lgu_name.data_containers.find(params[:container_id])
    @response = Hash.new
  end

  def update
    ActiveRecord::Base.transaction do
      if @data_container.update(permited_params)
        # do nothing
      else
        @errors = @data_container.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  private
  def permited_params
    ActionController::Parameters.new(params).require(:data_container).permit(:score_visualization, :benchmark_visualization, :comparison_visualization)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end
  
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @data_container
    else
      response[:success] = false
      response[:details] = error || @data_container.errors.full_messages.join(', ')
    end
  end

end
