LguCoordHandlerError = Class.new(StandardError)

class LguCoordHandler

  attr_accessor :current_user, :response

  def initialize(current_user)
    @current_user = current_user
    @response     = Hash.new
  end

  def update_lgu_coords
    return error_message if current_user.background_processes.not_finished.present?
      count = LguName.count
      @background_process = current_user.background_processes.build(total_process: count)

      if @background_process.save
        @background_process.reload
        count = 1
        LguName.all.map(&:id).each do |lgu_name_id|
          UpdateLguCoordsWorker.perform_async(@background_process.id, lgu_name_id, count)
          count += 1
        end
      else
        @errors = @background_process.errors.full_messages.join(', ')
      end

    create_response(@errors)
    self
  end

  private

  def error_message
    response[:success] = false
    response[:details] = "Please wait for update to be finished"
    self
  end

  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = "Update processing Please wait to take effect"
    else
      response[:success] = false
      response[:details] = error || @background_process.errors.full_messages.join(', ')
    end
  end

end
