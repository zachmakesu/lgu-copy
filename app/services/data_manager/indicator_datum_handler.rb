IndicatorDatumHandlerError = Class.new(StandardError)

class IndicatorDatumHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    fields = [ 
            :key_result_area_id,
            :name,
            :formula
            ]
    model = :indicator_datum

    ActiveRecord::Base.transaction do 
      @indicator_datum = IndicatorDatum.create(permited_params(model, fields))
      
      if @indicator_datum.errors.blank?
        @indicator_datum.reload
        LguName.all.each { |lgu_name| lgu_name.data_containers.create(indicator_datum_id: @indicator_datum.id) }
      else
        @errors = @indicator_datum.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def update
    fields = [ 
            :key_result_area_id,
            :name,
            :formula
            ]
    model = :indicator_datum

    @indicator_datum = IndicatorDatum.find_by(id: params[:id])
    return error_message if @indicator_datum.blank? || IndicatorDatum.count == 0
    ActiveRecord::Base.transaction do
      if @indicator_datum.update(permited_params(model, fields))
        # do nothing
      else
        @errors = @indicator_datum.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete
    @indicator_datum = IndicatorDatum.find_by(id: params[:id])
    return error_message if @indicator_datum.blank? || IndicatorDatum.count == 0
    @indicator_datum.destroy

    create_response
    self
  end


  def create_income_class
    fields = [ 
            :income_class,
            :value
            ]
    model = :bench_mark

    @indicator_datum = IndicatorDatum.find_by(id: params[:id])
    return error_message if @indicator_datum.blank? || IndicatorDatum.count == 0
    
    ActiveRecord::Base.transaction do 
      @indicator_datum = @indicator_datum.bench_marks.create(permited_params(model, fields))
      
      if @indicator_datum.errors.blank?
        @indicator_datum.reload
      else
        @errors = @indicator_datum.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def update_income_class
    fields = [ 
            :income_class,
            :value
            ]
    model = :bench_mark

    @indicator_datum = IndicatorDatum.find_by(id: params[:id])
    @indicator_datum = @indicator_datum.bench_marks.find_by(id: params[:income_class_id])

    return error_message if @indicator_datum.blank? || IndicatorDatum.count == 0
    ActiveRecord::Base.transaction do
      if @indicator_datum.update(permited_params(model, fields))
        # do nothing
      else
        @errors = @indicator_datum.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete_income_class
    @indicator_datum = IndicatorDatum.find_by(id: params[:id])
    @indicator_datum = @indicator_datum.bench_marks.find_by(id: params[:income_class_id])

    return error_message if @indicator_datum.blank? || IndicatorDatum.count == 0
    @indicator_datum.destroy

    create_response
    self
  end

  def bulk_delete
    return error_message_ids if params[:ids].blank?  || IndicatorDatum.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = IndicatorDatum.find_by(id: id) 
      if datum.present?
        @indicator_datum = datum
        @indicator_datum.destroy
      end
    end

    (@indicator_datum = IndicatorDatum.new) if @indicator_datum.blank?
    create_response
    self
  end

  private

  def error_message_ids
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end
  
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @indicator_datum
    else
      response[:success] = false
      response[:details] = error || @indicator_datum.errors.full_messages.join(', ')
    end
  end

end
