KeyResultAreaHandlerError = Class.new(StandardError)

class KeyResultAreaHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    fields = [ 
            :indicator_type,
            :name
            ]
    model = :key_result_area

    ActiveRecord::Base.transaction do 
      @key_result_area = KeyResultArea.create(permited_params(model, fields))
      @errors = @key_result_area.errors.full_messages.join(', ')
    end
    create_response(@errors)
    self
  end

  def update
    fields = [ 
            :indicator_type,
            :name
            ]
    model = :key_result_area

    @key_result_area = KeyResultArea.find_by(id: params[:id])
    return error_message if @key_result_area.blank? || KeyResultArea.count == 0
    ActiveRecord::Base.transaction do
      if @key_result_area.update(permited_params(model, fields))
        # do nothing
      else
        @errors = @key_result_area.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete
    @key_result_area = KeyResultArea.find_by(id: params[:id])
    return error_message if @key_result_area.blank? || KeyResultArea.count == 0
    @key_result_area.destroy

    create_response
    self
  end

  def bulk_delete
    return error_message_ids if params[:ids].blank?  || KeyResultArea.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = KeyResultArea.find_by(id: id) 
      if datum.present?
        @key_result_area = datum
        @key_result_area.destroy
      end
    end

    (@key_result_area = KeyResultArea.new) if @key_result_area.blank?
    create_response
    self
  end

  private

  def error_message_ids
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end

  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end
  
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @key_result_area
    else
      response[:success] = false
      response[:details] = error || @key_result_area.errors.full_messages.join(', ')
    end
  end

end
