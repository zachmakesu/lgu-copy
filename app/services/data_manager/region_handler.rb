RegionHandlerError = Class.new(StandardError)

class RegionHandler

  attr_accessor :params, :response

  def initialize(params)
    @params   = params
    @response = Hash.new
  end

  def create
    fields = [ 
            :name
            ]
    model = :region

    ActiveRecord::Base.transaction do 
      @region = Region.create(permited_params(model, fields))
      @errors = @region.errors.full_messages.join(', ')
    end
    create_response(@errors)
    self
  end

  def update
    fields = [ 
            :name
            ]
    model = :region

    @region = Region.find_by(id: params[:id])
    return error_message if @region.blank? || Region.count == 0
    ActiveRecord::Base.transaction do
      if @region.update(permited_params(model, fields))
        # do nothing
      else
        @errors = @region.errors.full_messages.join(', ')
      end
    end
    create_response(@errors)
    self
  end

  def delete
    @region = Region.find_by(id: params[:id])
    return error_message if @region.blank? || Region.count == 0
    @region.destroy

    create_response
    self
  end

  def bulk_delete
    return error_message_ids if params[:ids].blank?  || Region.count == 0

    ids = params[:ids].split('^')
    ids = ids.uniq.map {|i| i.to_i }.uniq
    ids.each do |id| 
      datum = Region.find_by(id: id) 
      if datum.present?
        @region = datum
        @region.destroy
      end
    end

    (@region = Region.new) if @region.blank?
    create_response
    self
  end

  private

  def error_message_ids
    response[:success] = false
    response[:details] = "Invalid Ids"
    self
  end
  
  def permited_params(model_class, model_fields)
    ActionController::Parameters.new(params).require(model_class).permit(model_fields)
  end

  def error_message
    response[:success] = false
    response[:details] = "Invalid Id"
    self
  end
  
  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = @region
    else
      response[:success] = false
      response[:details] = error || @region.errors.full_messages.join(', ')
    end
  end

end
