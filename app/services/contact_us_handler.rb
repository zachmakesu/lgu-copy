ContactUsHandlerError = Class.new(StandardError)

class ContactUsHandler
  attr_accessor :user, :params, :response

  def initialize(params)
    @params   = params
    @user     = User.not_deleted.find_by(employee_id: params[:employee_id])
    @response = Hash.new
  end

  def send
    return error_message if invalid_params?
    return error_email if !valid_email?
    message = ContactUsMailer.send_message(contact_options).deliver_now
    create_response(message.errors)
    self
  end

  private

  def contact_options
    {
      email: params[:email],
      name: params[:name],
      message: params[:message]
    }
  end

  def create_response(error=nil)
    if error.blank?
      response[:success] = true
      response[:details] = "Message sending success."
    else
      response[:success] = false
      response[:details] = "Message sending failed."
    end
  end

  def error_message
    response[:success] = false
    response[:details] = "name, email and message required"
    self
  end

  def error_email
    response[:success] = false
    response[:details] = "Invalid email"
    self
  end

  def invalid_params?
    params[:email].blank? || params[:name].blank? || params[:message].blank?
  end

  def valid_email?
    params[:email] =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  end
end
