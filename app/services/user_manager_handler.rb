UserManagerHandlerError = Class.new(StandardError)

class UserManagerHandler
  include ApiHelper
  attr_accessor :user, :params, :response

  def initialize(params)
    @user       = User.find_by(employee_id: params[:employee_id])
    @params     = params
    @response   = Hash.new
  end

  def create_user
    return error_message if missing_params?
    ActiveRecord::Base.transaction do
      @user = User.create(create_params)
      if params[:image].present?
          photo = @user.photos.profile.build()
          photo.image = build_attachment(params[:image])
          photo.position = 0
          photo.default_type = 0
          photo.save  
      end
    end

    create_response
    self
  end

  def update_user
    return error_message if update_missing_params? || user.blank?

    ActiveRecord::Base.transaction do
      if @user.update_attributes(update_params)
        if params[:image].present?
          avatar = @user.photos.profile.find_by(position: 0)
          avatar.update(position: 1) if avatar

          photo = @user.photos.build()
          photo.image = build_attachment(params[:image])
          photo.position = 0
          photo.default_type = 0
          photo.save  
        end
        @user.reload
      else
        @errors = @user.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def deactivate
    return invalid_id if @user.blank?
    ActiveRecord::Base.transaction do
      if @user.update_attributes(deleted_at: DateTime.now)
        @user.reload
      else
        @errors = @user.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end

  def activate
    return invalid_id if @user.blank?
    ActiveRecord::Base.transaction do
      if @user.update_attributes(deleted_at: nil)
        @user.reload
      else
        @errors = @user.errors.full_messages.join(', ')
      end
    end

    create_response(@errors)
    self
  end


  private

  def create_params
    employee_id = "C#{'%04d' % (User.count+1)}"
    input_params = {  
      employee_id:        employee_id,
      password:           "password123",
      password_confirmation: "password123",
      email:              params[:email],
      first_name:         params[:first_name],
      last_name:          params[:last_name],
      role:               params[:role]
    }
  end

  def update_params
    input_params = {  
      password:           params[:password],
      password_confirmation: params[:password],
      email:              params[:email],
      first_name:         params[:first_name],
      last_name:          params[:last_name],
      role:               params[:role]
    }
  end

  def error_message
    response[:success] = false
    response[:details] = "Missing parameters"
    self
  end

  def invalid_id
    response[:success] = false
    response[:details] = "Invalid employee id"
    self
  end

  def missing_params?
    params[:email].blank? || params[:first_name].blank? || params[:last_name].blank? || params[:role].blank? 
  end

  def update_missing_params?
    params[:email].blank? || params[:first_name].blank? || params[:last_name].blank? || params[:role].blank? || params[:password].blank?
  end

  def create_response(error=nil)
    if !@user.nil? && @user.errors.blank?
      response[:success] = true
      response[:details] = @user
    else
      response[:success] = false
      response[:details] = error || @user.errors.full_messages.uniq.join(', ')
    end
  end
end
