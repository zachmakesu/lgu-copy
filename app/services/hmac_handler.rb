class HmacHandler
  attr_accessor :endpoint, :params, :version
  WHITELISTED_ATTR = %w{ file image background_image csv_file }

  def initialize endpoint, params, version=1
    @endpoint = endpoint
    @params = params
    @version = version
  end

  # https://instagram.com/developer/secure-api-requests/?hl=en
  def self.signature_from(endpoint, params, api_version=1)
    date = Time.now.to_s[0,10] #string format (yyyy-mm-dd) ex. "2016-06-25"
    hmac_secret_key = ENV["HMAC_SECRET_#{api_version}"]
    secret = "#{hmac_secret_key}_#{date}"
    sig = endpoint

    params.sort.map do |key, val|
      next if WHITELISTED_ATTR.include?(key)
      
      new_val = if val.is_a? Hash
                  val.map do |k,v| 
                    if v.is_a? Array
                      new_v = v.join('^')
                      "#{k}=#{new_v}" 
                    else
                      "#{k}=#{v}" 
                    end
                  end.join('&') 
                else
                  val
                end
      sig += '|%s=%s' % [key, new_val]
    end

    # require "pry"
    # binding.pry
    
    digest = OpenSSL::Digest.new('sha256')
    return OpenSSL::HMAC.hexdigest(digest, secret, sig)
  end

  # http://www.rubydoc.info/github/plataformatec/devise/Devise.secure_compare
  def self.secure_compare(a, b)
    return false if a.blank? || b.blank? || a.bytesize != b.bytesize
    l = a.unpack "C#{a.bytesize}"

    res = 0
    b.each_byte { |byte| res |= byte ^ l.shift }
    res == 0
  end
  
  def digest
    HmacHandler.signature_from(@endpoint, @params, @version)
  end

end
