ForgotPasswordHandlerError = Class.new(StandardError)

class ForgotPasswordHandler
  attr_accessor :user, :params, :response

  def initialize(params)
    @params   = params
    @user     = User.find_by(email: params[:email])
    @response = Hash.new
  end

  def reset_password
    return default_error if user.blank?
    user.send_reset_password_instructions
    create_response("Reset password has been sent to #{params[:email]}")
    self
  end

  private
  def create_response(responses)
    response[:success] = true
    response[:details] = responses 
  end

  def default_error
    response[:success] = false
    response[:details] = "Invalid email"
    self
  end
end
