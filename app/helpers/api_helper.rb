module ApiHelper
  extend Grape::API::Helpers

  # TODO: Change references to utc
  # Why name format :utc if it returns :iso8601?
  Grape::Entity.format_with :utc do |date|
    date.to_datetime if date.present?
  end

  Grape::Entity.format_with :mdy do |date|
    date.to_datetime.strftime("%B %d, %Y") if date.present?
  end

  Grape::Entity.format_with :utc_time do |time|
    time.to_s(:time) if time.present?
  end

  Grape::Entity.format_with :date_announcement do |date|
    date.to_date if date.present?
  end

  ['original','thumb','medium','large','xlarge'].each do |size|
    Grape::Entity.format_with "#{size}_photo_url".to_sym do |photo|
      photo.image.url(size).to_s if photo.present?
    end
  end
 
  def authorize!(current_user, permission)
    error!({messages: '401 Unauthorized permission' },401) unless permission.include?(current_user.role)
  end

  def allow_cors
   headers["Access-Control-Allow-Origin"] = "*"
   headers["Access-Control-Allow-Methods"] = %w{GET POST PUT DELETE}.join(",")
   headers["Access-Control-Allow-Headers"] = %w{Origin Accept Content-Type X-Requested-With X-CSRF-Token}.join(",")
   head(:ok) if request.request_method == "OPTIONS"
  end

  def permited_params(model, model_fields)
    ActionController::Parameters.new(params).require(model).permit(model_fields)
  end

  def build_attachment(image)
    attachment =  {
      :filename => image[:filename],
      :type => image[:type],
      :headers => image[:head],
      :tempfile => image[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end

  def build_attachment_file(file)
    attachment =  {
      :filename => file[:filename],
      :type => file[:type],
      :headers => file[:head],
      :tempfile => file[:tempfile]
    }
    ActionDispatch::Http::UploadedFile.new(attachment)
  end

  def ensure_date(date = nil) #yyyy-mm
    date = "#{Time.now.to_s[0,7]}" if date.blank? 
    y, m = date.split '-'
    d = 1
    if Date.valid_date? y.to_i, m.to_i, d.to_i
      Time.parse("#{date[0,7]}-01") >= Time.now ? "" : date[0,7]
    else
      ""
    end
  end
  
  def validate_year_format!(yyyy) #yyyy
    error!({messages: 'Invalid year format' },401) unless Date.strptime("#{yyyy.to_i}", "%Y").gregorian?
  end
end
