class AddFinishedToCsvFile < ActiveRecord::Migration
  def change
    add_column :csv_files, :finished, :boolean, default: false
  end
end
