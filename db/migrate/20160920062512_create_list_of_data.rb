class CreateListOfData < ActiveRecord::Migration
  def change
    create_table :list_of_data do |t|
      t.references :indicator, index: true, foreign_key: true
      t.integer    :indicator_datum_id, null: false
      t.timestamps null: false
    end
  end
end
