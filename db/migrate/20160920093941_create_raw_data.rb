class CreateRawData < ActiveRecord::Migration
  def change
    create_table :raw_data do |t|
      t.references :data_container, index: true, foreign_key: true
      t.integer    :year, null: false
      t.string     :datum
      t.timestamps null: false
    end
  end
end
