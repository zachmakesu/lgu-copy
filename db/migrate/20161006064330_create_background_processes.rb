class CreateBackgroundProcesses < ActiveRecord::Migration
  def change
    create_table :background_processes do |t|
      t.integer    :processable_id 
      t.string     :processable_type
      t.integer    :total_process, default: 0
      t.integer    :total_processed, default: 0

      t.timestamps null: false
    end
  end
end
