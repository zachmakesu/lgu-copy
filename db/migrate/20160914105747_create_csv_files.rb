class CreateCsvFiles < ActiveRecord::Migration
  def change
    create_table :csv_files do |t| 
      t.attachment :csv
      t.integer    :csvable_id 
      t.string     :csvable_type

      t.timestamps null: false
    end
  end
end
