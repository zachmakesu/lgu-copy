class CreateDataContainers < ActiveRecord::Migration
  def change
    create_table :data_containers do |t|
      t.references :lgu_name, index: true, foreign_key: true
      t.integer    :indicator_datum_id, null: false
      t.integer    :score_visualization, null: false, default: 0
      t.integer    :benchmark_visualization, null: false, default: 0
      t.integer    :comparison_visualization, null: false, default: 0

      t.timestamps null: false
    end
  end
end
