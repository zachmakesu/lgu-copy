class AddIndicatorDatumReferencesInListOfData < ActiveRecord::Migration
  def change
    remove_column :list_of_data, :indicator_datum_id
    add_reference :list_of_data, :indicator_datum, index: true, foreign_key: true
  end
end
