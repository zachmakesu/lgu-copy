class CreateApiKeys < ActiveRecord::Migration
  def change
    create_table :api_keys do |t|
      t.integer    :user_id
      t.string     :encrypted_access_token, null: false, default: ""
      t.boolean    :active, null: false, default: true
      t.datetime   :expires_at
      
      t.timestamps null: false
    end
  end
end
