class UpdateDataContainerAttrs < ActiveRecord::Migration
  def change
    remove_column :data_containers, :benchmark_visualization
    remove_column :data_containers, :comparison_visualization
    remove_column :data_containers, :score_visualization

    add_column :data_containers, :benchmark_visualization, :string, null: false, default: "pie"
    add_column :data_containers, :comparison_visualization, :string, null: false, default: "pie"
    add_column :data_containers, :score_visualization, :string, null: false, default: "pie"
  end
end
