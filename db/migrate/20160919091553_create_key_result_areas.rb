class CreateKeyResultAreas < ActiveRecord::Migration
  def change
    create_table :key_result_areas do |t|
      t.string :name, null: false
      t.integer :indicator_type, null: false, default: 0
      
      t.timestamps null: false
    end
  end
end
