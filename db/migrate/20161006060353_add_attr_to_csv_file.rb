class AddAttrToCsvFile < ActiveRecord::Migration
  def change
    add_column :csv_files, :total_rows, :integer, default: 0
    add_column :csv_files, :total_processed_rows, :integer, default: 0
  end
end
