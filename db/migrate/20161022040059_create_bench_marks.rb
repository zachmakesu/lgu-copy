class CreateBenchMarks < ActiveRecord::Migration
  def change
    create_table :bench_marks do |t|
      t.references :indicator_datum, index: true, foreign_key: true
      t.string :income_class , null: false
      t.string :value
      t.timestamps null: false
    end
  end
end
