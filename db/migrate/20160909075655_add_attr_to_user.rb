class AddAttrToUser < ActiveRecord::Migration
  def change
  add_column :users, :employee_id,                         :string, null: false
  add_column :users, :first_name,                          :string
  add_column :users, :last_name,                           :string
  add_column :users, :role,                                :integer, default: 0, null: false
  add_column :users, :deleted_at,                          :string
  end
end
