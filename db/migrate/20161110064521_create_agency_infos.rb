class CreateAgencyInfos < ActiveRecord::Migration
  def change
    create_table :agency_infos do |t|
      t.string      :telephone_number
      t.string      :mobile_number
      t.string      :email
      t.text        :address
      t.text        :about_us
      t.timestamps null: false
    end
  end
end
