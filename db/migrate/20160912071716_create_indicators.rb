class CreateIndicators < ActiveRecord::Migration
  def change
    create_table :indicators do |t|
      t.references :lgu_name, index: true, foreign_key: true
      t.string     :name, null: false
      t.integer    :indicator_type, null: false, default: 0
      t.integer    :score_visualization, null: false, default: 0
      t.integer    :benchmark_visualization, null: false, default: 0
      t.integer    :comparison_visualization, null: false, default: 0
      t.timestamps null: false
    end
  end
end
