class AddLatLangInLgu < ActiveRecord::Migration
  def change
    add_column :lgu_names, :latitude, :float
    add_column :lgu_names, :longitude, :float
  end
end
