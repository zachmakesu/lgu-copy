class AddGeocodeToProvince < ActiveRecord::Migration
  def change
    add_column :provinces, :geocode, :string
  end
end
