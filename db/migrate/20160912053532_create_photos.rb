class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.attachment :image
      t.boolean   :default, null: false, default: false
      t.integer   :position, null: false
      t.integer   :default_type, null: false, default: 0
      t.integer   :imageable_id
      t.string    :imageable_type

      t.timestamps null: false
    end
  end
end
