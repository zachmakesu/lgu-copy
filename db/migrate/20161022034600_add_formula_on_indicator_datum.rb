class AddFormulaOnIndicatorDatum < ActiveRecord::Migration
  def change
    add_column :indicator_data, :formula, :string
  end
end
