class CreateLguNames < ActiveRecord::Migration
  def change
    create_table :lgu_names do |t|
      t.string   :name, null: false
      t.integer  :lgu_type_id
      t.integer  :region_id
      t.integer  :province_id

      t.string  :income_class
      t.timestamps null: false
    end
  end
end
