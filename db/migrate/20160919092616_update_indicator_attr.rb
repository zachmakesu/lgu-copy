class UpdateIndicatorAttr < ActiveRecord::Migration
  def change
    remove_column :indicators, :lgu_name_id
    remove_column :indicators, :indicator_type
    add_reference :indicators, :key_result_area, index: true, foreign_key: true

    remove_column :indicators, :benchmark_visualization
    remove_column :indicators, :comparison_visualization
    remove_column :indicators, :score_visualization
    
  end
end
