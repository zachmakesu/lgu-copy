class RemoveLguTypeIdInRegion < ActiveRecord::Migration
  def change
    remove_column :regions, :lgu_type_id
  end
end
