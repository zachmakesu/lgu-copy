def create_employee_id
  employee_id = "C#{'%04d' % (User.count+1)}"
end
users = [
  {
    role: 1,
    first_name: "Admin",
    last_name: "Admin",
    employee_id: create_employee_id,
    email: "lgu@gorated.ph",
    password: "password123"
  }
]

ActiveRecord::Base.transaction do
  if User.all.empty?
    users.each do |user|
      new_user = User.new
      new_user.role                  = user[:role]
      new_user.first_name            = user[:first_name]
      new_user.last_name             = user[:last_name]
      new_user.employee_id           = user[:employee_id]
      new_user.email                 = user[:email]
      new_user.password              = user[:password]
    
      if new_user.save
        print '✓'
      else
        puts new_user.errors.inspect
        break
      end
    end
    print "\nTotal : #{User.all.count}\n"
  else
    print "Skipped seeding user table.\n"
  end
end
