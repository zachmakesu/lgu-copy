key_result_areas = [
  {
    indicator_type: "financial",
    name: "Revenue Generation Capacity"
  },
  {
    indicator_type: "financial",
    name: "Local Collection Growth"
  },
  {
    indicator_type: "financial",
    name: "Expenditure Management"
  },
  {
    indicator_type: "non_financial",
    name: "Electronic Statement Receipts and Expenditures (eSRE) Report"
  },
  {
    indicator_type: "non_financial",
    name: "Schedule of Market Values (SMV) Updating"
  },
  {
    indicator_type: "non_financial",
    name: "Quarterly Report on Real Property Assessments (QRRPA)"
  }
]

ActiveRecord::Base.transaction do
  if KeyResultArea.all.empty?
    key_result_areas.each do |kra|
      new_kra = KeyResultArea.new
      new_kra.indicator_type  = kra[:indicator_type]
      new_kra.name            = kra[:name]
    
      if new_kra.save
        print '✓'
      else
        puts new_kra.errors.inspect
        break
      end
    end
    print "\nTotal : #{KeyResultArea.all.count}\n"
  else
    print "Skipped seeding key_result_area table.\n"
  end
end
