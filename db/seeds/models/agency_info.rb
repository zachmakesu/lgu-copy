agency_info = [
  {
    telephone_number: "02 123-4567",
    mobile_number: "0920-230-400",
    email: "info@blgf.gov.ph",
    address: "6th floor DOF Building, Roxas Boulevard",
    about_us: "<p>The LGU Fiscal Sustainability Scorecard aims to institutionalize the regular publication of ﬁscal indicators and performance review of the LGUs in the spirit of accountability and good local financial housekeeping.</p>"
  }
]

ActiveRecord::Base.transaction do
  if AgencyInfo.all.empty?
    agency_info.each do |info|
      new_info = AgencyInfo.new(info)
      if new_info.save
        print '✓'
      else
        puts new_info.errors.inspect
        break
      end
    end
    print "\nTotal : #{AgencyInfo.all.count}\n"
  else
    print "Skipped seeding agency_info table.\n"
  end
end
