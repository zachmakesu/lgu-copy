# Add seed file for all envs here
models = %w{ users key_result_areas agency_info } 

models.each do |model|
  puts '#' * 80
  puts "Table: #{model.upcase}"
  require_relative "models/#{model}.rb"
end
