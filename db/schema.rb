# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170306035036) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "agency_infos", force: :cascade do |t|
    t.string   "telephone_number"
    t.string   "mobile_number"
    t.string   "email"
    t.text     "address"
    t.text     "about_us"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "api_keys", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "encrypted_access_token", default: "",   null: false
    t.boolean  "active",                 default: true, null: false
    t.datetime "expires_at"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  create_table "background_processes", force: :cascade do |t|
    t.integer  "processable_id"
    t.string   "processable_type"
    t.integer  "total_process",    default: 0
    t.integer  "total_processed",  default: 0
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
  end

  create_table "bench_marks", force: :cascade do |t|
    t.integer  "indicator_datum_id"
    t.string   "income_class",       null: false
    t.string   "value"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
  end

  add_index "bench_marks", ["indicator_datum_id"], name: "index_bench_marks_on_indicator_datum_id", using: :btree

  create_table "csv_files", force: :cascade do |t|
    t.string   "csv_file_name"
    t.string   "csv_content_type"
    t.integer  "csv_file_size"
    t.datetime "csv_updated_at"
    t.integer  "csvable_id"
    t.string   "csvable_type"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "total_rows",           default: 0
    t.integer  "total_processed_rows", default: 0
    t.boolean  "finished",             default: false
  end

  create_table "data_containers", force: :cascade do |t|
    t.integer  "lgu_name_id"
    t.integer  "indicator_datum_id",                       null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "benchmark_visualization",  default: "pie", null: false
    t.string   "comparison_visualization", default: "pie", null: false
    t.string   "score_visualization",      default: "pie", null: false
  end

  add_index "data_containers", ["lgu_name_id"], name: "index_data_containers_on_lgu_name_id", using: :btree

  create_table "indicator_data", force: :cascade do |t|
    t.string   "name",               null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "key_result_area_id"
    t.string   "formula"
  end

  create_table "indicators", force: :cascade do |t|
    t.string   "name",               null: false
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "key_result_area_id"
  end

  add_index "indicators", ["key_result_area_id"], name: "index_indicators_on_key_result_area_id", using: :btree

  create_table "key_result_areas", force: :cascade do |t|
    t.string   "name",                       null: false
    t.integer  "indicator_type", default: 0, null: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "lgu_names", force: :cascade do |t|
    t.string   "name",         null: false
    t.integer  "lgu_type_id"
    t.integer  "region_id"
    t.integer  "province_id"
    t.string   "income_class"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "lgu_code"
  end

  create_table "lgu_types", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "list_of_data", force: :cascade do |t|
    t.integer  "indicator_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "indicator_datum_id"
  end

  add_index "list_of_data", ["indicator_datum_id"], name: "index_list_of_data_on_indicator_datum_id", using: :btree
  add_index "list_of_data", ["indicator_id"], name: "index_list_of_data_on_indicator_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.boolean  "default",            default: false, null: false
    t.integer  "position",                           null: false
    t.integer  "default_type",       default: 0,     null: false
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "image_belong_to"
  end

  create_table "provinces", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "geocode"
  end

  create_table "raw_data", force: :cascade do |t|
    t.integer  "data_container_id"
    t.integer  "year",              null: false
    t.string   "datum"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "raw_data", ["data_container_id"], name: "index_raw_data_on_data_container_id", using: :btree

  create_table "regions", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "employee_id",                         null: false
    t.string   "first_name"
    t.string   "last_name"
    t.integer  "role",                   default: 0,  null: false
    t.string   "deleted_at"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  add_foreign_key "bench_marks", "indicator_data"
  add_foreign_key "data_containers", "lgu_names"
  add_foreign_key "indicators", "key_result_areas"
  add_foreign_key "list_of_data", "indicator_data"
  add_foreign_key "list_of_data", "indicators"
  add_foreign_key "raw_data", "data_containers"
end
